package pt.ulusofona.heilab.xperis.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import pt.ulusofona.heilab.xperis.IntegrationTest;
import pt.ulusofona.heilab.xperis.domain.DataMimeType;
import pt.ulusofona.heilab.xperis.repository.DataMimeTypeRepository;

/**
 * Integration tests for the {@link DataMimeTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DataMimeTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_EXTENSION = "AAAAAAAAAA";
    private static final String UPDATED_FILE_EXTENSION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/data-mime-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private DataMimeTypeRepository dataMimeTypeRepository;

    @Autowired
    private MockMvc restDataMimeTypeMockMvc;

    private DataMimeType dataMimeType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataMimeType createEntity() {
        DataMimeType dataMimeType = new DataMimeType()
            .type(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .fileExtension(DEFAULT_FILE_EXTENSION);
        return dataMimeType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataMimeType createUpdatedEntity() {
        DataMimeType dataMimeType = new DataMimeType()
            .type(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .fileExtension(UPDATED_FILE_EXTENSION);
        return dataMimeType;
    }

    @BeforeEach
    public void initTest() {
        dataMimeTypeRepository.deleteAll();
        dataMimeType = createEntity();
    }

    @Test
    void createDataMimeType() throws Exception {
        int databaseSizeBeforeCreate = dataMimeTypeRepository.findAll().size();
        // Create the DataMimeType
        restDataMimeTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataMimeType)))
            .andExpect(status().isCreated());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeCreate + 1);
        DataMimeType testDataMimeType = dataMimeTypeList.get(dataMimeTypeList.size() - 1);
        assertThat(testDataMimeType.getType()).isEqualTo(DEFAULT_NAME);
        assertThat(testDataMimeType.getFileExtension()).isEqualTo(DEFAULT_FILE_EXTENSION);
    }

    @Test
    void createDataMimeTypeWithExistingId() throws Exception {
        // Create the DataMimeType with an existing ID
        dataMimeType.setId("existing_id");

        int databaseSizeBeforeCreate = dataMimeTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataMimeTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataMimeType)))
            .andExpect(status().isBadRequest());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataMimeTypeRepository.findAll().size();
        // set the field null
        dataMimeType.setType(null);

        // Create the DataMimeType, which fails.

        restDataMimeTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataMimeType)))
            .andExpect(status().isBadRequest());

        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkFileExtensionIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataMimeTypeRepository.findAll().size();
        // set the field null
        dataMimeType.setFileExtension(null);

        // Create the DataMimeType, which fails.

        restDataMimeTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataMimeType)))
            .andExpect(status().isBadRequest());

        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllDataMimeTypes() throws Exception {
        // Initialize the database
        dataMimeTypeRepository.save(dataMimeType);

        // Get all the dataMimeTypeList
        restDataMimeTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataMimeType.getId())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].fileExtension").value(hasItem(DEFAULT_FILE_EXTENSION)));
    }

    @Test
    void getDataMimeType() throws Exception {
        // Initialize the database
        dataMimeTypeRepository.save(dataMimeType);

        // Get the dataMimeType
        restDataMimeTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, dataMimeType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dataMimeType.getId()))
            .andExpect(jsonPath("$.type").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.fileExtension").value(DEFAULT_FILE_EXTENSION));
    }

    @Test
    void getNonExistingDataMimeType() throws Exception {
        // Get the dataMimeType
        restDataMimeTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewDataMimeType() throws Exception {
        // Initialize the database
        dataMimeTypeRepository.save(dataMimeType);

        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();

        // Update the dataMimeType
        DataMimeType updatedDataMimeType = dataMimeTypeRepository.findById(dataMimeType.getId()).get();
        updatedDataMimeType.type(UPDATED_NAME).fileExtension(UPDATED_FILE_EXTENSION);

        restDataMimeTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDataMimeType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDataMimeType))
            )
            .andExpect(status().isOk());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
        DataMimeType testDataMimeType = dataMimeTypeList.get(dataMimeTypeList.size() - 1);
        assertThat(testDataMimeType.getType()).isEqualTo(UPDATED_NAME);
        assertThat(testDataMimeType.getFileExtension()).isEqualTo(UPDATED_FILE_EXTENSION);
    }

    @Test
    void putNonExistingDataMimeType() throws Exception {
        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();
        dataMimeType.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDataMimeTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dataMimeType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dataMimeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchDataMimeType() throws Exception {
        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();
        dataMimeType.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataMimeTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dataMimeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamDataMimeType() throws Exception {
        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();
        dataMimeType.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataMimeTypeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataMimeType)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateDataMimeTypeWithPatch() throws Exception {
        // Initialize the database
        dataMimeTypeRepository.save(dataMimeType);

        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();

        // Update the dataMimeType using partial update
        DataMimeType partialUpdatedDataMimeType = new DataMimeType();
        partialUpdatedDataMimeType.setId(dataMimeType.getId());

        partialUpdatedDataMimeType.fileExtension(UPDATED_FILE_EXTENSION);

        restDataMimeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDataMimeType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDataMimeType))
            )
            .andExpect(status().isOk());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
        DataMimeType testDataMimeType = dataMimeTypeList.get(dataMimeTypeList.size() - 1);
        assertThat(testDataMimeType.getType()).isEqualTo(DEFAULT_NAME);
        assertThat(testDataMimeType.getFileExtension()).isEqualTo(UPDATED_FILE_EXTENSION);
    }

    @Test
    void fullUpdateDataMimeTypeWithPatch() throws Exception {
        // Initialize the database
        dataMimeTypeRepository.save(dataMimeType);

        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();

        // Update the dataMimeType using partial update
        DataMimeType partialUpdatedDataMimeType = new DataMimeType();
        partialUpdatedDataMimeType.setId(dataMimeType.getId());

        partialUpdatedDataMimeType.type(UPDATED_NAME).fileExtension(UPDATED_FILE_EXTENSION);

        restDataMimeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDataMimeType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDataMimeType))
            )
            .andExpect(status().isOk());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
        DataMimeType testDataMimeType = dataMimeTypeList.get(dataMimeTypeList.size() - 1);
        assertThat(testDataMimeType.getType()).isEqualTo(UPDATED_NAME);
        assertThat(testDataMimeType.getFileExtension()).isEqualTo(UPDATED_FILE_EXTENSION);
    }

    @Test
    void patchNonExistingDataMimeType() throws Exception {
        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();
        dataMimeType.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDataMimeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dataMimeType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dataMimeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchDataMimeType() throws Exception {
        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();
        dataMimeType.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataMimeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dataMimeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamDataMimeType() throws Exception {
        int databaseSizeBeforeUpdate = dataMimeTypeRepository.findAll().size();
        dataMimeType.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataMimeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dataMimeType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DataMimeType in the database
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteDataMimeType() throws Exception {
        // Initialize the database
        dataMimeTypeRepository.save(dataMimeType);

        int databaseSizeBeforeDelete = dataMimeTypeRepository.findAll().size();

        // Delete the dataMimeType
        restDataMimeTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, dataMimeType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DataMimeType> dataMimeTypeList = dataMimeTypeRepository.findAll();
        assertThat(dataMimeTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

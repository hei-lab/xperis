package pt.ulusofona.heilab.xperis.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pt.ulusofona.heilab.xperis.domain.DataMimeType;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.repository.ExperimentMetaDataRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentRepository;

/**
 * Integration tests for the {@link ExperimentMetaDataResource} REST controller.
 */
class ExperimentMetaDataResourceIT extends BasicResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_STRUCTURE = "AAAAAAAAAA";
    private static final String UPDATED_DATA_STRUCTURE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/experiment-meta-data";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static final String HEXADECIMAL_ID = "000000000000000000000000";

    @Autowired
    private ExperimentMetaDataRepository experimentMetaDataRepository;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private MockMvc restExperimentMetaDataMockMvc;

    private ExperimentMetaData experimentMetaData;
    private Experiment experiment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExperimentMetaData createEntity() {
        ExperimentMetaData experimentMetaData = new ExperimentMetaData()
            .description(DEFAULT_DESCRIPTION)
            .dataStructure(DEFAULT_DATA_STRUCTURE);
        // Add required entity
        Experiment experiment;
        experiment = ExperimentResourceIT.createEntity();
        experiment.setId(HEXADECIMAL_ID);
        experimentMetaData.setExperiment(experiment);
        // Add required entity
        DataMimeType dataMimeType;
        dataMimeType = pt.ulusofona.heilab.xperis.web.rest.DataMimeTypeResourceIT.createEntity();
        dataMimeType.setId(HEXADECIMAL_ID);
        experimentMetaData.setDataMimeType(dataMimeType);
        return experimentMetaData;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExperimentMetaData createUpdatedEntity() {
        ExperimentMetaData experimentMetaData = new ExperimentMetaData()
            .description(UPDATED_DESCRIPTION)
            .dataStructure(UPDATED_DATA_STRUCTURE);
        // Add required entity
        Experiment experiment;
        experiment = ExperimentResourceIT.createUpdatedEntity();
        experiment.setId(HEXADECIMAL_ID);
        experimentMetaData.setExperiment(experiment);
        // Add required entity
        DataMimeType dataMimeType;
        dataMimeType = pt.ulusofona.heilab.xperis.web.rest.DataMimeTypeResourceIT.createUpdatedEntity();
        dataMimeType.setId(HEXADECIMAL_ID);
        experimentMetaData.setDataMimeType(dataMimeType);
        return experimentMetaData;
    }

    @BeforeEach
    public void initTest() {
        experimentMetaDataRepository.deleteAll();
        experimentMetaData = createEntity();
        experiment = (experiment == null ? experimentRepository.save(experimentMetaData.getExperiment()) : experiment);
    }

    @Test
    void createExperimentMetaData() throws Exception {
        int databaseSizeBeforeCreate = experimentMetaDataRepository.findAll().size();
        // Create the ExperimentMetaData
        restExperimentMetaDataMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isCreated());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeCreate + 1);
        ExperimentMetaData testExperimentMetaData = experimentMetaDataList.get(experimentMetaDataList.size() - 1);
        assertThat(testExperimentMetaData.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testExperimentMetaData.getDataStructure()).isEqualTo(DEFAULT_DATA_STRUCTURE);
    }

    @Test
    void createExperimentMetaDataWithExistingId() throws Exception {
        // Create the ExperimentMetaData with an existing ID
        experimentMetaData.setId("existing_id");

        int databaseSizeBeforeCreate = experimentMetaDataRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExperimentMetaDataMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = experimentMetaDataRepository.findAll().size();
        // set the field null
        experimentMetaData.setDescription(null);

        // Create the ExperimentMetaData, which fails.

        restExperimentMetaDataMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isBadRequest());

        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllExperimentMetaData() throws Exception {
        // Initialize the database
        experimentMetaDataRepository.save(experimentMetaData);

        // Get all the experimentMetaDataList
        restExperimentMetaDataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(experimentMetaData.getId())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].dataStructure").value(hasItem(DEFAULT_DATA_STRUCTURE.toString())));
    }

    @Test
    void getExperimentMetaData() throws Exception {
        // Initialize the database
        experimentMetaDataRepository.save(experimentMetaData);

        // Get the experimentMetaData
        restExperimentMetaDataMockMvc
            .perform(get(ENTITY_API_URL_ID, experimentMetaData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(experimentMetaData.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.dataStructure").value(DEFAULT_DATA_STRUCTURE.toString()));
    }

    @Test
    void getNonExistingExperimentMetaData() throws Exception {
        // Get the experimentMetaData
        restExperimentMetaDataMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewExperimentMetaData() throws Exception {
        // Initialize the database
        experimentMetaDataRepository.save(experimentMetaData);

        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();

        // Update the experimentMetaData
        ExperimentMetaData updatedExperimentMetaData = experimentMetaDataRepository.findById(experimentMetaData.getId()).get();
        updatedExperimentMetaData.description(UPDATED_DESCRIPTION).dataStructure(UPDATED_DATA_STRUCTURE);

        restExperimentMetaDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedExperimentMetaData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedExperimentMetaData))
            )
            .andExpect(status().isOk());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
        ExperimentMetaData testExperimentMetaData = experimentMetaDataList.get(experimentMetaDataList.size() - 1);
        assertThat(testExperimentMetaData.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testExperimentMetaData.getDataStructure()).isEqualTo(UPDATED_DATA_STRUCTURE);
    }

    @Test
    void putNonExistingExperimentMetaData() throws Exception {
        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();
        experimentMetaData.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExperimentMetaDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, experimentMetaData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchExperimentMetaData() throws Exception {
        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();
        experimentMetaData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMetaDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamExperimentMetaData() throws Exception {
        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();
        experimentMetaData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMetaDataMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateExperimentMetaDataWithPatch() throws Exception {
        // Initialize the database
        experimentMetaDataRepository.save(experimentMetaData);

        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();

        // Update the experimentMetaData using partial update
        ExperimentMetaData partialUpdatedExperimentMetaData = new ExperimentMetaData();
        partialUpdatedExperimentMetaData.setId(experimentMetaData.getId());

        partialUpdatedExperimentMetaData.description(UPDATED_DESCRIPTION).dataStructure(UPDATED_DATA_STRUCTURE);

        restExperimentMetaDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExperimentMetaData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExperimentMetaData))
            )
            .andExpect(status().isOk());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
        ExperimentMetaData testExperimentMetaData = experimentMetaDataList.get(experimentMetaDataList.size() - 1);
        assertThat(testExperimentMetaData.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testExperimentMetaData.getDataStructure()).isEqualTo(UPDATED_DATA_STRUCTURE);
    }

    @Test
    void fullUpdateExperimentMetaDataWithPatch() throws Exception {
        // Initialize the database
        experimentMetaDataRepository.save(experimentMetaData);

        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();

        // Update the experimentMetaData using partial update
        ExperimentMetaData partialUpdatedExperimentMetaData = new ExperimentMetaData();
        partialUpdatedExperimentMetaData.setId(experimentMetaData.getId());

        partialUpdatedExperimentMetaData.description(UPDATED_DESCRIPTION).dataStructure(UPDATED_DATA_STRUCTURE);

        restExperimentMetaDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExperimentMetaData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExperimentMetaData))
            )
            .andExpect(status().isOk());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
        ExperimentMetaData testExperimentMetaData = experimentMetaDataList.get(experimentMetaDataList.size() - 1);
        assertThat(testExperimentMetaData.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testExperimentMetaData.getDataStructure()).isEqualTo(UPDATED_DATA_STRUCTURE);
    }

    @Test
    void patchNonExistingExperimentMetaData() throws Exception {
        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();
        experimentMetaData.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExperimentMetaDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, experimentMetaData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchExperimentMetaData() throws Exception {
        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();
        experimentMetaData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMetaDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamExperimentMetaData() throws Exception {
        int databaseSizeBeforeUpdate = experimentMetaDataRepository.findAll().size();
        experimentMetaData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMetaDataMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experimentMetaData))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExperimentMetaData in the database
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteExperimentMetaData() throws Exception {
        // Initialize the database
        experimentMetaDataRepository.save(experimentMetaData);

        int databaseSizeBeforeDelete = experimentMetaDataRepository.findAll().size();

        // Delete the experimentMetaData
        restExperimentMetaDataMockMvc
            .perform(delete(ENTITY_API_URL_ID, experimentMetaData.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExperimentMetaData> experimentMetaDataList = experimentMetaDataRepository.findAll();
        assertThat(experimentMetaDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

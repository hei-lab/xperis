package pt.ulusofona.heilab.xperis.web.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.domain.enumeration.ExperimentStatus;
import pt.ulusofona.heilab.xperis.repository.ExperimentRepository;
import pt.ulusofona.heilab.xperis.service.ExperimentService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ExperimentResource} REST controller.
 */
class ExperimentResourceIT extends BasicResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_FINISH_CAPTURE_DATE = Instant.now().plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.MILLIS);
    private static final Instant UPDATED_FINISH_CAPTURE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_FINISH_DATE = Instant.now().plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.MILLIS);
    private static final Instant UPDATED_FINISH_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ExperimentStatus DEFAULT_STATUS = ExperimentStatus.STARTED;
    private static final ExperimentStatus UPDATED_STATUS = ExperimentStatus.FINISHED;

    private static final String ENTITY_API_URL = "/api/experiments";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ExperimentRepository experimentRepository;

    @Mock
    private ExperimentService experimentServiceMock;

    @Autowired
    private MockMvc restExperimentMockMvc;

    private Experiment experiment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Experiment createEntity() {
        Experiment experiment = new Experiment()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .startDate(DEFAULT_START_DATE)
            .finishCaptureDate(DEFAULT_FINISH_CAPTURE_DATE)
            .finishDate(DEFAULT_FINISH_DATE)
            .status(DEFAULT_STATUS);
        experiment.setCreatedBy(createUser().getLogin());
        return experiment;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Experiment createUpdatedEntity() {
        Experiment experiment = new Experiment()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .startDate(UPDATED_START_DATE)
            .finishCaptureDate(UPDATED_FINISH_CAPTURE_DATE)
            .finishDate(UPDATED_FINISH_DATE)
            .status(UPDATED_STATUS);
        experiment.setCreatedBy(createUser().getLogin());
        return experiment;
    }

    @BeforeEach
    public void initTest() {
        experimentRepository.deleteAll();
        experiment = createEntity();
    }

    @Test
    void createExperiment() throws Exception {
        int databaseSizeBeforeCreate = experimentRepository.findAll().size();
        // Create the Experiment
        restExperimentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experiment)))
            .andExpect(status().isCreated());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeCreate + 1);
        Experiment testExperiment = experimentList.get(experimentList.size() - 1);
        assertThat(testExperiment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExperiment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testExperiment.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testExperiment.getFinishCaptureDate()).isEqualTo(DEFAULT_FINISH_CAPTURE_DATE);
        assertThat(testExperiment.getFinishDate()).isEqualTo(DEFAULT_FINISH_DATE);
        assertThat(testExperiment.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createExperimentWithExistingId() throws Exception {
        // Create the Experiment with an existing ID
        experiment.setId("existing_id");

        int databaseSizeBeforeCreate = experimentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExperimentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experiment)))
            .andExpect(status().isBadRequest());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = experimentRepository.findAll().size();
        // set the field null
        experiment.setName(null);

        // Create the Experiment, which fails.

        restExperimentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experiment)))
            .andExpect(status().isBadRequest());

        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllExperiments() throws Exception {
        // Initialize the database
        experimentRepository.save(experiment);

        // Get all the experimentList
        restExperimentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(experiment.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishCaptureDate").value(hasItem(DEFAULT_FINISH_CAPTURE_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishDate").value(hasItem(DEFAULT_FINISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllExperimentsWithEagerRelationshipsIsEnabled() throws Exception {
        when(experimentServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restExperimentMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(experimentServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllExperimentsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(experimentServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restExperimentMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(experimentServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getExperiment() throws Exception {
        // Initialize the database
        experimentRepository.save(experiment);

        // Get the experiment
        restExperimentMockMvc
            .perform(get(ENTITY_API_URL_ID, experiment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(experiment.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.finishCaptureDate").value(DEFAULT_FINISH_CAPTURE_DATE.toString()))
            .andExpect(jsonPath("$.finishDate").value(DEFAULT_FINISH_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    void getNonExistingExperiment() throws Exception {
        // Get the experiment
        restExperimentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewExperiment() throws Exception {
        // Initialize the database
        experimentRepository.save(experiment);

        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();

        // Update the experiment
        Experiment updatedExperiment = experimentRepository.findById(experiment.getId()).get();
        updatedExperiment
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .startDate(UPDATED_START_DATE)
            .finishCaptureDate(UPDATED_FINISH_CAPTURE_DATE)
            .finishDate(UPDATED_FINISH_DATE)
            .status(UPDATED_STATUS);

        restExperimentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedExperiment.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedExperiment))
            )
            .andExpect(status().isOk());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
        Experiment testExperiment = experimentList.get(experimentList.size() - 1);
        assertThat(testExperiment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExperiment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testExperiment.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testExperiment.getFinishCaptureDate()).isEqualTo(UPDATED_FINISH_CAPTURE_DATE);
        assertThat(testExperiment.getFinishDate()).isEqualTo(UPDATED_FINISH_DATE);
        assertThat(testExperiment.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingExperiment() throws Exception {
        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();
        experiment.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExperimentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, experiment.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(experiment))
            )
            .andExpect(status().isBadRequest());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchExperiment() throws Exception {
        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();
        experiment.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(experiment))
            )
            .andExpect(status().isBadRequest());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamExperiment() throws Exception {
        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();
        experiment.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experiment)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateExperimentWithPatch() throws Exception {
        // Initialize the database
        experimentRepository.save(experiment);

        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();

        // Update the experiment using partial update
        Experiment partialUpdatedExperiment = new Experiment();
        partialUpdatedExperiment.setId(experiment.getId());

        partialUpdatedExperiment.finishCaptureDate(UPDATED_FINISH_CAPTURE_DATE).finishDate(UPDATED_FINISH_DATE);

        restExperimentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExperiment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExperiment))
            )
            .andExpect(status().isOk());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
        Experiment testExperiment = experimentList.get(experimentList.size() - 1);
        assertThat(testExperiment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExperiment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testExperiment.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testExperiment.getFinishCaptureDate()).isEqualTo(UPDATED_FINISH_CAPTURE_DATE);
        assertThat(testExperiment.getFinishDate()).isEqualTo(UPDATED_FINISH_DATE);
        assertThat(testExperiment.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void fullUpdateExperimentWithPatch() throws Exception {
        // Initialize the database
        experimentRepository.save(experiment);

        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();

        // Update the experiment using partial update
        Experiment partialUpdatedExperiment = new Experiment();
        partialUpdatedExperiment.setId(experiment.getId());

        partialUpdatedExperiment
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .startDate(UPDATED_START_DATE)
            .finishCaptureDate(UPDATED_FINISH_CAPTURE_DATE)
            .finishDate(UPDATED_FINISH_DATE)
            .status(UPDATED_STATUS);

        restExperimentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExperiment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExperiment))
            )
            .andExpect(status().isOk());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
        Experiment testExperiment = experimentList.get(experimentList.size() - 1);
        assertThat(testExperiment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExperiment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testExperiment.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testExperiment.getFinishCaptureDate()).isEqualTo(UPDATED_FINISH_CAPTURE_DATE);
        assertThat(testExperiment.getFinishDate()).isEqualTo(UPDATED_FINISH_DATE);
        assertThat(testExperiment.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingExperiment() throws Exception {
        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();
        experiment.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExperimentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, experiment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experiment))
            )
            .andExpect(status().isBadRequest());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchExperiment() throws Exception {
        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();
        experiment.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experiment))
            )
            .andExpect(status().isBadRequest());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamExperiment() throws Exception {
        int databaseSizeBeforeUpdate = experimentRepository.findAll().size();
        experiment.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(experiment))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Experiment in the database
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteExperiment() throws Exception {
        // Initialize the database
        experimentRepository.save(experiment);

        int databaseSizeBeforeDelete = experimentRepository.findAll().size();

        // Delete the experiment
        restExperimentMockMvc
            .perform(delete(ENTITY_API_URL_ID, experiment.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Experiment> experimentList = experimentRepository.findAll();
        assertThat(experimentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package pt.ulusofona.heilab.xperis.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.domain.ExperimentData;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentMetaDataRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentRepository;

/**
 * Integration tests for the {@link ExperimentDataResource} REST controller.
 */
class ExperimentDataResourceIT extends BasicResourceIT {

    private static final String DEFAULT_PARTICIPANTS = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANTS = "BBBBBBBBBB";

    private static final byte[] DEFAULT_DATA = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_DATA = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_DATA_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_DATA_CONTENT_TYPE = "image/png";

    private static final String ENTITY_API_URL = "/api/experiment-data";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static final String HEXADECIMAL_ID = "000000000000000000000000";

    @Autowired
    private ExperimentDataRepository experimentDataRepository;

    @Autowired
    private ExperimentMetaDataRepository experimentMetaDataRepository;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private MockMvc restExperimentDataMockMvc;

    private ExperimentData experimentData;
    private ExperimentMetaData experimentMetaData;
    private Experiment experiment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExperimentData createEntity() {
        ExperimentData experimentData = new ExperimentData()
            .participants(DEFAULT_PARTICIPANTS)
            .data(DEFAULT_DATA)
            .dataContentType(DEFAULT_DATA_CONTENT_TYPE);
        // Add required entity
        ExperimentMetaData experimentMetaData;
        experimentMetaData = ExperimentMetaDataResourceIT.createEntity();
        experimentMetaData.setId(HEXADECIMAL_ID);
        experimentData.setExperimentMetaData(experimentMetaData);
        return experimentData;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExperimentData createUpdatedEntity() {
        ExperimentData experimentData = new ExperimentData()
            .participants(UPDATED_PARTICIPANTS)
            .data(UPDATED_DATA)
            .dataContentType(UPDATED_DATA_CONTENT_TYPE);
        // Add required entity
        ExperimentMetaData experimentMetaData;
        experimentMetaData = ExperimentMetaDataResourceIT.createUpdatedEntity();
        experimentMetaData.setId(HEXADECIMAL_ID);
        experimentData.setExperimentMetaData(experimentMetaData);
        return experimentData;
    }

    @BeforeEach
    public void initTest() {
        experimentDataRepository.deleteAll();
        experimentData = createEntity();
        experimentMetaData =
            (experimentMetaData == null ? experimentMetaDataRepository.save(experimentData.getExperimentMetaData()) : experimentMetaData);
        experiment = (experiment == null ? experimentRepository.save(experimentMetaData.getExperiment()) : experiment);
    }

    @Test
    void createExperimentData() throws Exception {
        int databaseSizeBeforeCreate = experimentDataRepository.findAll().size();
        // Create the ExperimentData
        restExperimentDataMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isCreated());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeCreate + 1);
        ExperimentData testExperimentData = experimentDataList.get(experimentDataList.size() - 1);
        assertThat(testExperimentData.getParticipants()).isEqualTo(DEFAULT_PARTICIPANTS);
        assertThat(testExperimentData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testExperimentData.getDataContentType()).isEqualTo(DEFAULT_DATA_CONTENT_TYPE);
    }

    @Test
    void createExperimentDataWithExistingId() throws Exception {
        // Create the ExperimentData with an existing ID
        experimentData.setId("existing_id");

        int databaseSizeBeforeCreate = experimentDataRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExperimentDataMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllExperimentData() throws Exception {
        // Initialize the database
        experimentDataRepository.save(experimentData);

        // Get all the experimentDataList
        restExperimentDataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(experimentData.getId())))
            .andExpect(jsonPath("$.[*].participants").value(hasItem(DEFAULT_PARTICIPANTS)))
            .andExpect(jsonPath("$.[*].dataContentType").value(hasItem(DEFAULT_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].data").value(hasItem(Base64Utils.encodeToString(DEFAULT_DATA))));
    }

    @Test
    void getExperimentData() throws Exception {
        // Initialize the database
        experimentDataRepository.save(experimentData);

        // Get the experimentData
        restExperimentDataMockMvc
            .perform(get(ENTITY_API_URL_ID, experimentData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(experimentData.getId()))
            .andExpect(jsonPath("$.participants").value(DEFAULT_PARTICIPANTS))
            .andExpect(jsonPath("$.dataContentType").value(DEFAULT_DATA_CONTENT_TYPE))
            .andExpect(jsonPath("$.data").value(Base64Utils.encodeToString(DEFAULT_DATA)));
    }

    @Test
    void getNonExistingExperimentData() throws Exception {
        // Get the experimentData
        restExperimentDataMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewExperimentData() throws Exception {
        // Initialize the database
        experimentDataRepository.save(experimentData);

        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();

        // Update the experimentData
        ExperimentData updatedExperimentData = experimentDataRepository.findById(experimentData.getId()).get();
        updatedExperimentData.participants(UPDATED_PARTICIPANTS).data(UPDATED_DATA).dataContentType(UPDATED_DATA_CONTENT_TYPE);

        restExperimentDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedExperimentData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedExperimentData))
            )
            .andExpect(status().isOk());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
        ExperimentData testExperimentData = experimentDataList.get(experimentDataList.size() - 1);
        assertThat(testExperimentData.getParticipants()).isEqualTo(UPDATED_PARTICIPANTS);
        assertThat(testExperimentData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testExperimentData.getDataContentType()).isEqualTo(UPDATED_DATA_CONTENT_TYPE);
    }

    @Test
    void putNonExistingExperimentData() throws Exception {
        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();
        experimentData.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExperimentDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, experimentData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchExperimentData() throws Exception {
        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();
        experimentData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamExperimentData() throws Exception {
        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();
        experimentData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentDataMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(experimentData)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateExperimentDataWithPatch() throws Exception {
        // Initialize the database
        experimentDataRepository.save(experimentData);

        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();

        // Update the experimentData using partial update
        ExperimentData partialUpdatedExperimentData = new ExperimentData();
        partialUpdatedExperimentData.setId(experimentData.getId());

        partialUpdatedExperimentData.participants(UPDATED_PARTICIPANTS);

        restExperimentDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExperimentData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExperimentData))
            )
            .andExpect(status().isOk());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
        ExperimentData testExperimentData = experimentDataList.get(experimentDataList.size() - 1);
        assertThat(testExperimentData.getParticipants()).isEqualTo(UPDATED_PARTICIPANTS);
        assertThat(testExperimentData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testExperimentData.getDataContentType()).isEqualTo(DEFAULT_DATA_CONTENT_TYPE);
    }

    @Test
    void fullUpdateExperimentDataWithPatch() throws Exception {
        // Initialize the database
        experimentDataRepository.save(experimentData);

        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();

        // Update the experimentData using partial update
        ExperimentData partialUpdatedExperimentData = new ExperimentData();
        partialUpdatedExperimentData.setId(experimentData.getId());

        partialUpdatedExperimentData.participants(UPDATED_PARTICIPANTS).data(UPDATED_DATA).dataContentType(UPDATED_DATA_CONTENT_TYPE);

        restExperimentDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExperimentData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExperimentData))
            )
            .andExpect(status().isOk());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
        ExperimentData testExperimentData = experimentDataList.get(experimentDataList.size() - 1);
        assertThat(testExperimentData.getParticipants()).isEqualTo(UPDATED_PARTICIPANTS);
        assertThat(testExperimentData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testExperimentData.getDataContentType()).isEqualTo(UPDATED_DATA_CONTENT_TYPE);
    }

    @Test
    void patchNonExistingExperimentData() throws Exception {
        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();
        experimentData.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExperimentDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, experimentData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchExperimentData() throws Exception {
        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();
        experimentData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamExperimentData() throws Exception {
        int databaseSizeBeforeUpdate = experimentDataRepository.findAll().size();
        experimentData.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExperimentDataMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(experimentData))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExperimentData in the database
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteExperimentData() throws Exception {
        // Initialize the database
        experimentDataRepository.save(experimentData);

        int databaseSizeBeforeDelete = experimentDataRepository.findAll().size();

        // Delete the experimentData
        restExperimentDataMockMvc
            .perform(delete(ENTITY_API_URL_ID, experimentData.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExperimentData> experimentDataList = experimentDataRepository.findAll();
        assertThat(experimentDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

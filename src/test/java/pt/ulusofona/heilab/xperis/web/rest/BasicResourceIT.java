package pt.ulusofona.heilab.xperis.web.rest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import pt.ulusofona.heilab.xperis.IntegrationTest;
import pt.ulusofona.heilab.xperis.domain.Authority;
import pt.ulusofona.heilab.xperis.domain.User;
import pt.ulusofona.heilab.xperis.repository.UserRepository;
import pt.ulusofona.heilab.xperis.security.AuthoritiesConstants;

import java.util.Collections;

/**
 * Integration tests for the {@link ExperimentResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser(username = "master", roles = { "MASTER" })
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BasicResourceIT {

    @Autowired
    private UserRepository userRepository;

    public static User createUser() {
        User user = new User();
        user.setLogin("master");
        user.setPassword("ABCDEFGHIJ1234567890ABCDEFGHIJ1234567890ABCDEFGHIJ1234567890");
        user.setAuthorities(Collections.singleton(new Authority(AuthoritiesConstants.MASTER)));
        return user;
    }

    @BeforeAll
    public void init() {
        userRepository.save(createUser());
    }
}

package pt.ulusofona.heilab.xperis.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import pt.ulusofona.heilab.xperis.web.rest.TestUtil;

class DataTemplateTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataTemplate.class);
        DataTemplate dataTemplate1 = new DataTemplate();
        dataTemplate1.setId("id1");
        DataTemplate dataTemplate2 = new DataTemplate();
        dataTemplate2.setId(dataTemplate1.getId());
        assertThat(dataTemplate1).isEqualTo(dataTemplate2);
        dataTemplate2.setId("id2");
        assertThat(dataTemplate1).isNotEqualTo(dataTemplate2);
        dataTemplate1.setId(null);
        assertThat(dataTemplate1).isNotEqualTo(dataTemplate2);
    }
}

package pt.ulusofona.heilab.xperis.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

public class InstitutionTest {

    private Institution institution;
    private User user1;
    private User user2;

    @BeforeEach
    public void setup() {
        institution = new Institution();
        institution.setId("1");
        institution.setName("Test Institution");

        user1 = new User();
        user1.setId("1");
        user1.setFirstName("John");
        user1.setLastName("Doe");

        user2 = new User();
        user2.setId("2");
        user2.setFirstName("Jane");
        user2.setLastName("Doe");
    }

    @Test
    public void testGettersSetters() {
        Assertions.assertEquals("1", institution.getId());
        Assertions.assertEquals("Test Institution", institution.getName());

        Set<User> employees = new HashSet<>();
        employees.add(user1);
        institution.setEmployees(employees);

        Assertions.assertEquals(employees, institution.getEmployees());
    }

    @Test
    public void testAddEmployees() {
        institution.addEmployees(user1);
        Assertions.assertTrue(institution.getEmployees().contains(user1));
    }

    @Test
    public void testRemoveEmployees() {
        institution.addEmployees(user1);
        institution.addEmployees(user2);

        institution.removeEmployees(user1);

        Assertions.assertFalse(institution.getEmployees().contains(user1));
        Assertions.assertTrue(institution.getEmployees().contains(user2));
    }

    @Test
    public void testEquals() {
        Institution sameInstitution = new Institution()
            .id("1")
            .name("Test Institution")
            .employees(new HashSet<>());

        Institution differentInstitution = new Institution();
        differentInstitution.setId("2");
        differentInstitution.setName("Different Institution");

        Assertions.assertEquals(sameInstitution, sameInstitution);
        Assertions.assertEquals(institution, sameInstitution);
        Assertions.assertNotEquals(institution, differentInstitution);
        Assertions.assertNotEquals(institution, null);
    }

    @Test
    public void testHashCode() {
        Institution sameInstitution = new Institution();
        sameInstitution.setId("1");
        sameInstitution.setName("Test Institution");

        Assertions.assertEquals(institution.hashCode(), sameInstitution.hashCode());
    }

    @Test
    public void testToString() {
        String expectedToString = "Institution{id=1, name='Test Institution'}";
        Assertions.assertEquals(expectedToString, institution.toString());
    }
}

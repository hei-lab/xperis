package pt.ulusofona.heilab.xperis.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pt.ulusofona.heilab.xperis.domain.enumeration.ExperimentStatus;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ExperimentTest {

    @Mock
    private Set<ExperimentMetaData> experimentMetaDataMock;

    @Mock
    private Set<User> membersMock;

    @Mock
    private Set<User> assistantsMock;

    @InjectMocks
    private Experiment experiment;

    @Test
    void testGetId() {
        experiment.setId("1");
        assertEquals("1", experiment.getId());
    }

    @Test
    void testGetName() {
        experiment.setName("Test Experiment");
        assertEquals("Test Experiment", experiment.getName());
    }

    @Test
    void testGetDescription() {
        experiment.setDescription("Experiment description");
        assertEquals("Experiment description", experiment.getDescription());
    }

    @Test
    void testGetStartDate() {
        Instant startDate = Instant.now();
        experiment.setStartDate(startDate);
        assertEquals(startDate, experiment.getStartDate());
    }

    @Test
    void testGetFinishCaptureDate() {
        Instant finishCaptureDate = Instant.now();
        experiment.setFinishCaptureDate(finishCaptureDate);
        assertEquals(finishCaptureDate, experiment.getFinishCaptureDate());
    }

    @Test
    void testGetFinishDate() {
        Instant finishDate = Instant.now();
        experiment.setFinishDate(finishDate);
        assertEquals(finishDate, experiment.getFinishDate());
    }

    @Test
    void testGetStatusWhenExperimentIsFinished() {
        Instant finishDate = Instant.now().minusSeconds(10);
        experiment.setFinishDate(finishDate);
        assertEquals(ExperimentStatus.FINISHED, experiment.getStatus());
    }

    @Test
    void testGetStatusWhenExperimentIsCaptureFinished() {
        Instant finishCaptureDate = Instant.now().minusSeconds(10);
        experiment.setFinishCaptureDate(finishCaptureDate);
        assertEquals(ExperimentStatus.CAPTURE_FINISHED, experiment.getStatus());
    }

    @Test
    void testGetStatusWhenExperimentIsStarted() {
        Instant startDate = Instant.now().minusSeconds(10);
        experiment.setStartDate(startDate);
        assertEquals(ExperimentStatus.STARTED, experiment.getStatus());
    }

    @Test
    void testGetStatusWhenExperimentIsCreated() {
        assertEquals(ExperimentStatus.CREATED, experiment.getStatus());
    }

    @Test
    void testGetExperimentMetaData() {
        Set<ExperimentMetaData> experimentMetaData = new HashSet<>();
        experimentMetaData.add(new ExperimentMetaData());
        experiment.setExperimentMetaData(experimentMetaData);
        assertEquals(experimentMetaData.size(), experiment.getExperimentMetaData().size());
    }

    @Test
    void testGetMembers() {
        Set<User> members = new HashSet<>();
        members.add(new User());
        experiment.setMembers(members);
        assertEquals(members.size(), experiment.getMembers().size());
    }

    @Test
    void testGetAssistants() {
        Set<User> assistants = new HashSet<>();
        assistants.add(new User());
        experiment.setAssistants(assistants);
        assertEquals(assistants.size(), experiment.getAssistants().size());
    }
}

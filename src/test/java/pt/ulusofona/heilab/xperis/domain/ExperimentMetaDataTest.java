package pt.ulusofona.heilab.xperis.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import pt.ulusofona.heilab.xperis.web.rest.TestUtil;

class ExperimentMetaDataTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExperimentMetaData.class);
        ExperimentMetaData experimentMetaData1 = new ExperimentMetaData();
        experimentMetaData1.setId("id1");
        ExperimentMetaData experimentMetaData2 = new ExperimentMetaData();
        experimentMetaData2.setId(experimentMetaData1.getId());
        assertThat(experimentMetaData1).isEqualTo(experimentMetaData2);
        experimentMetaData2.setId("id2");
        assertThat(experimentMetaData1).isNotEqualTo(experimentMetaData2);
        experimentMetaData1.setId(null);
        assertThat(experimentMetaData1).isNotEqualTo(experimentMetaData2);
    }
}

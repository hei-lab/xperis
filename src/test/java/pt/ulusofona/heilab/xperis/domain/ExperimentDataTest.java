package pt.ulusofona.heilab.xperis.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import pt.ulusofona.heilab.xperis.web.rest.TestUtil;

class ExperimentDataTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExperimentData.class);
        ExperimentData experimentData1 = new ExperimentData();
        experimentData1.setId("id1");
        ExperimentData experimentData2 = new ExperimentData();
        experimentData2.setId(experimentData1.getId());
        assertThat(experimentData1).isEqualTo(experimentData2);
        experimentData2.setId("id2");
        assertThat(experimentData1).isNotEqualTo(experimentData2);
        experimentData1.setId(null);
        assertThat(experimentData1).isNotEqualTo(experimentData2);
    }
}

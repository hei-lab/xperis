package pt.ulusofona.heilab.xperis.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import pt.ulusofona.heilab.xperis.web.rest.TestUtil;

class DataMimeTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataMimeType.class);
        DataMimeType dataMimeType1 = new DataMimeType();
        dataMimeType1.setId("id1");
        DataMimeType dataMimeType2 = new DataMimeType();
        dataMimeType2.setId(dataMimeType1.getId());
        assertThat(dataMimeType1).isEqualTo(dataMimeType2);
        dataMimeType2.setId("id2");
        assertThat(dataMimeType1).isNotEqualTo(dataMimeType2);
        dataMimeType1.setId(null);
        assertThat(dataMimeType1).isNotEqualTo(dataMimeType2);
    }
}

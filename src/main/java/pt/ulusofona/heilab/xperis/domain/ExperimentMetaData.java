package pt.ulusofona.heilab.xperis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A ExperimentMetaData.
 */
@Document(collection = "experiment_meta_data")
public class ExperimentMetaData extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("description")
    private String description;

    @Field("data_structure")
    private String dataStructure;

    @DBRef(lazy = true)
    @Field("experimentData")
    @JsonIgnoreProperties(value = { "experimentMetaData", "data" }, allowSetters = true)
    private Set<ExperimentData> experimentData = new HashSet<>();

    @DBRef
    @Field("experiment")
    @JsonIgnoreProperties(value = { "experimentMetaData", "members", "assistants" }, allowSetters = true)
    private Experiment experiment;

    @DBRef
    @Field("dataMimeType")
    @JsonIgnoreProperties(value = { "experimentMetaData", "dataTemplates" }, allowSetters = true)
    private DataMimeType dataMimeType;

    @JsonInclude
    @Transient
    private List<ExperimentDataLog> logs;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ExperimentMetaData id(String id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return this.description;
    }

    public ExperimentMetaData description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataStructure() {
        return this.dataStructure;
    }

    public ExperimentMetaData dataStructure(String dataStructure) {
        this.dataStructure = dataStructure;
        return this;
    }

    public void setDataStructure(String dataStructure) {
        this.dataStructure = dataStructure;
    }

    public Set<ExperimentData> getExperimentData() {
        return this.experimentData;
    }

    public ExperimentMetaData experimentData(Set<ExperimentData> experimentData) {
        this.setExperimentData(experimentData);
        return this;
    }

    public ExperimentMetaData addExperimentData(ExperimentData experimentData) {
        this.experimentData.add(experimentData);
        experimentData.setExperimentMetaData(this);
        return this;
    }

    public ExperimentMetaData removeExperimentData(ExperimentData experimentData) {
        this.experimentData.remove(experimentData);
        experimentData.setExperimentMetaData(null);
        return this;
    }

    public void setExperimentData(Set<ExperimentData> experimentData) {
        if (this.experimentData != null) {
            this.experimentData.forEach(i -> i.setExperimentMetaData(null));
        }
        if (experimentData != null) {
            experimentData.forEach(i -> i.setExperimentMetaData(this));
        }
        this.experimentData = experimentData;
    }

    public Experiment getExperiment() {
        return this.experiment;
    }

    public ExperimentMetaData experiment(Experiment experiment) {
        this.setExperiment(experiment);
        return this;
    }

    public void setExperiment(Experiment experiment) {
        this.experiment = experiment;
    }

    public DataMimeType getDataMimeType() {
        return this.dataMimeType;
    }

    public ExperimentMetaData dataMimeType(DataMimeType dataMimeType) {
        this.setDataMimeType(dataMimeType);
        return this;
    }

    public void setDataMimeType(DataMimeType dataMimeType) {
        this.dataMimeType = dataMimeType;
    }

    public List<ExperimentDataLog> getLogs() {
        return logs;
    }

    public void setLogs(List<ExperimentDataLog> logs) {
        this.logs = logs;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public ExperimentMetaData addExperiencData(ExperimentData experimentData) {
        this.experimentData.add(experimentData);
        experimentData.setExperimentMetaData(this);
        return this;
    }

    public ExperimentMetaData removeExperiencData(ExperimentData experimentData) {
        this.experimentData.remove(experimentData);
        experimentData.setExperimentMetaData(null);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExperimentMetaData)) {
            return false;
        }
        return id != null && id.equals(((ExperimentMetaData) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExperimentMetaData{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", dataStructure='" + getDataStructure() + "'" +
            "}";
    }
}

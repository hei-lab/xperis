package pt.ulusofona.heilab.xperis.domain.enumeration;

/**
 * The ExperimentStatus enumeration.
 */
public enum ExperimentStatus {
    CREATED,
    STARTED,
    CAPTURE_FINISHED,
    FINISHED,
}

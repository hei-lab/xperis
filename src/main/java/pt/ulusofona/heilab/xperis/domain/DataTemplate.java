package pt.ulusofona.heilab.xperis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A DataTemplate.
 */
@Document(collection = "data_template")
public class DataTemplate extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("name")
    private String name;

    @Field("data_structure")
    private String dataStructure;

    @Field("viewer_classes")
    private String viewerClasses;

    @DBRef
    @Field("dataMimeType")
    @JsonIgnoreProperties(value = { "experimentMetaData", "dataTemplates" }, allowSetters = true)
    private DataMimeType dataMimeType;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DataTemplate id(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public DataTemplate name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataStructure() {
        return this.dataStructure;
    }

    public DataTemplate dataStructure(String dataStructure) {
        this.dataStructure = dataStructure;
        return this;
    }

    public void setDataStructure(String dataStructure) {
        this.dataStructure = dataStructure;
    }

    public String getViewerClasses() {
        return this.viewerClasses;
    }

    public DataTemplate viewerClasses(String viewerClasses) {
        this.viewerClasses = viewerClasses;
        return this;
    }

    public void setViewerClasses(String viewerClasses) {
        this.viewerClasses = viewerClasses;
    }

    public DataMimeType getDataMimeType() {
        return this.dataMimeType;
    }

    public DataTemplate dataMimeType(DataMimeType dataMimeType) {
        this.setDataMimeType(dataMimeType);
        return this;
    }

    public void setDataMimeType(DataMimeType dataMimeType) {
        this.dataMimeType = dataMimeType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DataTemplate)) {
            return false;
        }
        return id != null && id.equals(((DataTemplate) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DataTemplate{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", dataStructure='" + getDataStructure() + "'" +
            ", viewerClasses='" + getViewerClasses() + "'" +
            "}";
    }
}

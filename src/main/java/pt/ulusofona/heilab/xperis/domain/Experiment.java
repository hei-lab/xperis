package pt.ulusofona.heilab.xperis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import pt.ulusofona.heilab.xperis.domain.enumeration.ExperimentStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Experiment.
 */
@Document(collection = "experiment")
public class Experiment extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("name")
    private String name;

    @Field("description")
    private String description;

    @Field("start_date")
    private Instant startDate;

    @Field("finish_capture_date")
    private Instant finishCaptureDate;

    @Field("finish_date")
    private Instant finishDate;

    @Field("status")
    private ExperimentStatus status;

    @DBRef(lazy = true)
    @Field("experimentMetaData")
    @JsonIgnoreProperties(value = { "experimentData", "experiment" }, allowSetters = true)
    private Set<ExperimentMetaData> experimentMetaData = new HashSet<>();

    @DBRef(lazy = true)
    @Field("members")
    private Set<User> members = new HashSet<>();

    @DBRef(lazy = true)
    @Field("assistants")
    private Set<User> assistants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Experiment id(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Experiment name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public Experiment description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getStartDate() {
        return this.startDate;
    }

    public Experiment startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getFinishCaptureDate() {
        return this.finishCaptureDate;
    }

    public Experiment finishCaptureDate(Instant finishCaptureDate) {
        this.finishCaptureDate = finishCaptureDate;
        return this;
    }

    public void setFinishCaptureDate(Instant finishCaptureDate) {
        this.finishCaptureDate = finishCaptureDate;
    }

    public Instant getFinishDate() {
        return this.finishDate;
    }

    public Experiment finishDate(Instant finishDate) {
        this.finishDate = finishDate;
        return this;
    }

    public void setFinishDate(Instant finishDate) {
        this.finishDate = finishDate;
    }

    public ExperimentStatus getStatus() {
        if (this.finishDate != null && this.finishDate.isBefore(Instant.now())) {
            this.status = ExperimentStatus.FINISHED;
        } else if (this.finishCaptureDate != null && this.finishCaptureDate.isBefore(Instant.now())) {
            this.status = ExperimentStatus.CAPTURE_FINISHED;
        } else if (this.startDate != null && this.startDate.isBefore(Instant.now())) {
            this.status = ExperimentStatus.STARTED;
        } else {
            this.status = ExperimentStatus.CREATED;
        }
        return this.status;
    }

    public Experiment status(ExperimentStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(ExperimentStatus status) {
        this.status = status;
    }

    public Set<ExperimentMetaData> getExperimentMetaData() {
        return this.experimentMetaData;
    }

    public Experiment experimentMetaData(Set<ExperimentMetaData> experimentMetaData) {
        this.setExperimentMetaData(experimentMetaData);
        return this;
    }

    public Experiment addExperimentMetaData(ExperimentMetaData experimentMetaData) {
        this.experimentMetaData.add(experimentMetaData);
        experimentMetaData.setExperiment(this);
        return this;
    }

    public Experiment removeExperimentMetaData(ExperimentMetaData experimentMetaData) {
        this.experimentMetaData.remove(experimentMetaData);
        experimentMetaData.setExperiment(null);
        return this;
    }

    public void setExperimentMetaData(Set<ExperimentMetaData> experimentMetaData) {
        if (this.experimentMetaData != null) {
            this.experimentMetaData.forEach(i -> i.setExperiment(null));
        }
        if (experimentMetaData != null) {
            experimentMetaData.forEach(i -> i.setExperiment(this));
        }
        this.experimentMetaData = experimentMetaData;
    }

    public Set<User> getMembers() {
        return this.members;
    }

    public Experiment members(Set<User> users) {
        this.setMembers(users);
        return this;
    }

    public Experiment addMembers(User user) {
        this.members.add(user);
        return this;
    }

    public Experiment removeMembers(User user) {
        this.members.remove(user);
        return this;
    }

    public void setMembers(Set<User> users) {
        this.members = users;
    }

    public Set<User> getAssistants() {
        return this.assistants;
    }

    public Experiment assistants(Set<User> users) {
        this.setAssistants(users);
        return this;
    }

    public Experiment addAssistants(User user) {
        this.assistants.add(user);
        return this;
    }

    public Experiment removeAssistants(User user) {
        this.assistants.remove(user);
        return this;
    }

    public void setAssistants(Set<User> users) {
        this.assistants = users;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Experiment)) {
            return false;
        }
        return id != null && id.equals(((Experiment) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Experiment{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", finishCaptureDate='" + getFinishCaptureDate() + "'" +
            ", finishDate='" + getFinishDate() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}

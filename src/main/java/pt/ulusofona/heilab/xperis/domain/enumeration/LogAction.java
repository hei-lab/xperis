package pt.ulusofona.heilab.xperis.domain.enumeration;

public enum LogAction {
    UPDATE,
    UPDATE_DATA,
    DOWNLOAD_DATA,
}

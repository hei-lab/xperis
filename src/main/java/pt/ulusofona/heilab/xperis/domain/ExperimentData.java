package pt.ulusofona.heilab.xperis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A ExperimentData.
 */
@Document(collection = "experiment_data")
public class ExperimentData extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("participants")
    private String participants;

    @Field("data")
    private byte[] data;

    @Field("data_content_type")
    private String dataContentType;

    @DBRef
    @Field("experimentMetaData")
    @JsonIgnoreProperties(value = { "experimentData", "experiment" }, allowSetters = true)
    private ExperimentMetaData experimentMetaData;

    @JsonInclude
    @Transient
    private List<ExperimentDataLog> logs;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ExperimentData id(String id) {
        this.id = id;
        return this;
    }

    public String getParticipants() {
        return this.participants;
    }

    public ExperimentData participants(String participants) {
        this.participants = participants;
        return this;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public byte[] getData() {
        return this.data;
    }

    public ExperimentData data(byte[] data) {
        this.data = data;
        return this;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getDataContentType() {
        return this.dataContentType;
    }

    public ExperimentData dataContentType(String dataContentType) {
        this.dataContentType = dataContentType;
        return this;
    }

    public void setDataContentType(String dataContentType) {
        this.dataContentType = dataContentType;
    }

    public ExperimentMetaData getExperimentMetaData() {
        return this.experimentMetaData;
    }

    public ExperimentData experimentMetaData(ExperimentMetaData experimentMetaData) {
        this.setExperimentMetaData(experimentMetaData);
        return this;
    }

    public void setExperimentMetaData(ExperimentMetaData experimentMetaData) {
        this.experimentMetaData = experimentMetaData;
    }

    public List<ExperimentDataLog> getLogs() {
        return logs;
    }

    public void setLogs(List<ExperimentDataLog> logs) {
        this.logs = logs;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExperimentData)) {
            return false;
        }
        return id != null && id.equals(((ExperimentData) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExperimentData{" +
            "id=" + getId() +
            ", participants='" + getParticipants() + "'" +
            ", data='" + Arrays.toString(getData()) + "'" +
            ", dataContentType='" + getDataContentType() + "'" +
            "}";
    }
}

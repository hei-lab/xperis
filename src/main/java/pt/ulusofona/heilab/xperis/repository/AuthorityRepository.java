package pt.ulusofona.heilab.xperis.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import pt.ulusofona.heilab.xperis.domain.Authority;

/**
 * Spring Data MongoDB repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {}

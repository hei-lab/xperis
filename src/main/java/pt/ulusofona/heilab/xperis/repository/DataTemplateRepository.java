package pt.ulusofona.heilab.xperis.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.DataTemplate;

/**
 * Spring Data MongoDB repository for the DataTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataTemplateRepository extends MongoRepository<DataTemplate, String> {}

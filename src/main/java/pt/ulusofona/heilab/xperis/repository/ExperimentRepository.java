package pt.ulusofona.heilab.xperis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.Experiment;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Experiment entity.
 */
@Repository
public interface ExperimentRepository extends MongoRepository<Experiment, String> {

    @Query(fields = "{'_id': 1}", value = "{}")
    List<Experiment> findAllIds();

    @Query("{}")
    Page<Experiment> findAllWithEagerRelationships(Pageable pageable);

    @Query("{'id': ?0}")
    Optional<Experiment> findOneWithEagerRelationships(String id);

    @Query(fields = "{'_id': 1}", value =
        "{$or: [ {'created_by': ?0}, {$and: [ {'members.$id': ObjectId(?1)}, {'startDate': {$lt: new Date()}}, {'finishDate': {$gt: new Date()}} ]} ]}"
    )
    List<Experiment> findAllIdsForMembers(String login, String userId);

    @Query(fields = "{'_id': 1}", value =
        "{$and: [ {'assistants.$id': ObjectId(?0)}, {'startDate': {$lt: new Date()}}, {'finishDate': {$gt: new Date()}} ]}")
    List<Experiment> findAllIdsForViewers(String userId);

    @Query("{'id': {$in: ?0}}")
    Page<Experiment> findAllInList(List<String> ids, Pageable pageable);
}

package pt.ulusofona.heilab.xperis.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.DataMimeType;

/**
 * Spring Data MongoDB repository for the DataMimeType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataMimeTypeRepository extends MongoRepository<DataMimeType, String> {
    List<DataMimeType> findAllByOrderByTypeAsc();

    DataMimeType findFirstByType(String type);
}

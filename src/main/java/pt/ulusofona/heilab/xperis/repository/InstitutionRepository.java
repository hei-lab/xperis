package pt.ulusofona.heilab.xperis.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.Institution;

/**
 * Spring Data MongoDB repository for the Institution entity.
 */
@Repository
public interface InstitutionRepository extends MongoRepository<Institution, String> {
    @Query("{}")
    Page<Institution> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Institution> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Institution> findOneWithEagerRelationships(String id);
}

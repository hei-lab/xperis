package pt.ulusofona.heilab.xperis.repository;

import java.util.Set;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.ExperimentData;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;

/**
 * Spring Data MongoDB repository for the ExperimentData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExperimentDataRepository extends MongoRepository<ExperimentData, String> {
    Page<ExperimentData> findAllByExperimentMetaData(ExperimentMetaData experiment, Pageable pageable);

    Set<ExperimentData> findAllByExperimentMetaData(String id);

    @Query("{'experimentMetaData.$id': {$in: ?0} }")
    Page<ExperimentData> findAllInExperimentMetaDatas(Set<ObjectId> experimentMetaDataIds, Pageable pageable);

    void deleteAllByExperimentMetaData(String experimentMetaDataId);
}

package pt.ulusofona.heilab.xperis.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.ExperimentDataLog;

import java.util.List;

/**
 * Spring Data MongoDB repository for the ExperimentDataLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExperimentDataLogRepository extends MongoRepository<ExperimentDataLog, String> {
    List<ExperimentDataLog> findAllByExperimentDataId(String experimentdataid);
}

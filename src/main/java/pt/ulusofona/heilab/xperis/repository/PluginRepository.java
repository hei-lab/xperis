package pt.ulusofona.heilab.xperis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.Plugin;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Plugin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PluginRepository extends MongoRepository<Plugin, String> {
    Page<Plugin> findAllByActivatedTrue(Pageable pageable);

    List<Plugin> findAllByActivatedTrue();

    List<Plugin> findAllByDataMimeTypeAndActivatedTrue(String dataMimeTypeId);

    Optional<Plugin> findFirstByClassNameAndIdNot(String className, String id);
    Optional<Plugin> findFirstByFileNameAndIdNot(String fileName, String id);
    Optional<Plugin> findFirstByNameAndIdNot(String name, String id);

}

package pt.ulusofona.heilab.xperis.service.exception;

public class NotAuthorizedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotAuthorizedException() {
        super("Not Authorized");
    }
}

package pt.ulusofona.heilab.xperis.service;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.DataMimeType;
import pt.ulusofona.heilab.xperis.repository.DataMimeTypeRepository;

/**
 * Service Implementation for managing {@link DataMimeType}.
 */
@Service
public class DataMimeTypeService {

    private final Logger log = LoggerFactory.getLogger(DataMimeTypeService.class);

    private final DataMimeTypeRepository dataMimeTypeRepository;

    public DataMimeTypeService(DataMimeTypeRepository dataMimeTypeRepository) {
        this.dataMimeTypeRepository = dataMimeTypeRepository;
    }

    /**
     * Save a dataMimeType.
     *
     * @param dataMimeType the entity to save.
     * @return the persisted entity.
     */
    public DataMimeType save(DataMimeType dataMimeType) {
        log.debug("Request to save DataMimeType : {}", dataMimeType);
        return dataMimeTypeRepository.save(dataMimeType);
    }

    /**
     * Partially update a dataMimeType.
     *
     * @param dataMimeType the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DataMimeType> partialUpdate(DataMimeType dataMimeType) {
        log.debug("Request to partially update DataMimeType : {}", dataMimeType);

        return dataMimeTypeRepository
            .findById(dataMimeType.getId())
            .map(
                existingDataMimeType -> {
                    if (dataMimeType.getType() != null) {
                        existingDataMimeType.setType(dataMimeType.getType());
                    }
                    if (dataMimeType.getFileExtension() != null) {
                        existingDataMimeType.setFileExtension(dataMimeType.getFileExtension());
                    }

                    return existingDataMimeType;
                }
            )
            .map(dataMimeTypeRepository::save);
    }

    /**
     * Get all the dataMimeTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<DataMimeType> findAll(Pageable pageable) {
        log.debug("Request to get all DataMimeTypes");
        Page<DataMimeType> page;
        if (pageable.isUnpaged()) {
            List<DataMimeType> result = dataMimeTypeRepository.findAllByOrderByTypeAsc();
            page = ServiceUtils.convertToPage(result, null);
        } else {
            page = dataMimeTypeRepository.findAll(pageable);
        }
        return page;
    }

    /**
     * Get one dataMimeType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<DataMimeType> findOne(String id) {
        log.debug("Request to get DataMimeType : {}", id);
        return dataMimeTypeRepository.findById(id);
    }

    /**
     * Delete the dataMimeType by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete DataMimeType : {}", id);
        dataMimeTypeRepository.deleteById(id);
    }
}

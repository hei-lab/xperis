package pt.ulusofona.heilab.xperis.service.mapper;

import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.ExperimentData;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.service.dto.ExperimentDataDTO;
import pt.ulusofona.heilab.xperis.service.dto.ExperimentMetaDataDTO;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link ExperimentMetaData} and its DTO called {@link ExperimentMetaDataDTO}.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class ExperimentMetaDataMapper {

    public Set<ExperimentMetaDataDTO> metaDatatoDTO(Set<ExperimentMetaData> experimentMetaDatas) {
        return experimentMetaDatas.stream().filter(Objects::nonNull).map(this::metaDatatoDTO).collect(Collectors.toSet());
    }

    public ExperimentMetaDataDTO metaDatatoDTO(ExperimentMetaData experimentMetaData) {
        return ExperimentMetaDataDTO
            .builder()
            .id(experimentMetaData.getId())
            .description(experimentMetaData.getDescription())
            .dataStructure(experimentMetaData.getDataStructure())
            .experimentData(datatoDTO(experimentMetaData.getExperimentData()))
            .build();
    }

    public Set<ExperimentDataDTO> datatoDTO(Set<ExperimentData> experimentDatas) {
        return experimentDatas.stream().filter(Objects::nonNull).map(this::datatoDTO).collect(Collectors.toSet());
    }

    public ExperimentDataDTO datatoDTO(ExperimentData experimentData) {
        return ExperimentDataDTO
            .builder()
            .id(experimentData.getId())
            .participants(experimentData.getParticipants())
            .createdDate(experimentData.getCreatedDate())
            .data(experimentData.getData())
            .build();
    }
}

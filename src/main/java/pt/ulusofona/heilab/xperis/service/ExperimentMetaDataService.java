package pt.ulusofona.heilab.xperis.service;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.domain.ExperimentData;
import pt.ulusofona.heilab.xperis.domain.ExperimentDataLog;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.domain.enumeration.LogAction;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataLogRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentMetaDataRepository;
import pt.ulusofona.heilab.xperis.service.dto.ExperimentMetaDataDTO;
import pt.ulusofona.heilab.xperis.service.exception.NotAuthorizedException;
import pt.ulusofona.heilab.xperis.service.mapper.ExperimentMetaDataMapper;
import pt.ulusofona.heilab.xperis.service.spi.PluginViewer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Service Implementation for managing {@link ExperimentMetaData}.
 */
@Service
public class ExperimentMetaDataService {

    private final Logger log = LoggerFactory.getLogger(ExperimentMetaDataService.class);

    private final ExperimentMetaDataRepository experimentMetaDataRepository;
    private final ExperimentMetaDataMapper experimentMetaDataMapper;
    private final ExperimentDataRepository experimentDataRepository;
    private final ExperimentDataLogRepository logRepository;
    private final ExperimentService experimentService;
    private final PluginLoader pluginLoader;

    public ExperimentMetaDataService(
        ExperimentMetaDataRepository experimentMetaDataRepository,
        ExperimentMetaDataMapper experimentMetaDataMapper,
        ExperimentDataRepository experimentDataRepository,
        ExperimentDataLogRepository logRepository,
        ExperimentService experimentService,
        PluginLoader pluginLoader
    ) {
        this.experimentMetaDataRepository = experimentMetaDataRepository;
        this.experimentMetaDataMapper = experimentMetaDataMapper;
        this.experimentDataRepository = experimentDataRepository;
        this.logRepository = logRepository;
        this.experimentService = experimentService;
        this.pluginLoader = pluginLoader;
    }

    /**
     * Save a experimentMetaData.
     *
     * @param experimentMetaData the entity to save.
     * @return the persisted entity.
     */
    public ExperimentMetaData save(ExperimentMetaData experimentMetaData) {
        if (!canUpdate(experimentMetaData)) {
            throw new NotAuthorizedException();
        }
        log.debug("Request to save ExperimentMetaData : {}", experimentMetaData);

        ExperimentMetaData newMetaData = experimentMetaDataRepository.save(experimentMetaData);
        experimentService.addExperimentMetaData(experimentMetaData.getExperiment().getId(), newMetaData);

        return newMetaData;
    }

    /**
     * Partially update a experimentMetaData.
     *
     * @param experimentMetaData the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ExperimentMetaData> partialUpdate(ExperimentMetaData experimentMetaData) {
        log.debug("Request to partially update ExperimentMetaData : {}", experimentMetaData);

        return experimentMetaDataRepository
            .findById(experimentMetaData.getId())
            .map(
                existingExperimentMetaData -> {
                    if (!canUpdate(existingExperimentMetaData)) {
                        throw new NotAuthorizedException();
                    }
                    if (experimentMetaData.getDescription() != null) {
                        existingExperimentMetaData.setDescription(experimentMetaData.getDescription());
                    }
                    if (experimentMetaData.getDataStructure() != null) {
                        existingExperimentMetaData.setDataStructure(experimentMetaData.getDataStructure());
                    }
                    if (experimentMetaData.getDataMimeType() != null) {
                        existingExperimentMetaData.setDataMimeType(experimentMetaData.getDataMimeType());
                    }
                    insertLog(new ExperimentDataLog(existingExperimentMetaData.getId(), LogAction.UPDATE));

                    Set<ExperimentData> experimentData = experimentDataRepository.findAllByExperimentMetaData(existingExperimentMetaData.getId());
                    existingExperimentMetaData.setExperimentData(experimentData);

                    return existingExperimentMetaData;
                }
            )
            .map(experimentMetaDataRepository::save);
    }

    /**
     * Get all the experimentMetaData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<ExperimentMetaData> findAll(Pageable pageable) {
        log.debug("Request to get all ExperimentMetaData");

        List<String> ids = experimentService.findAllIds();
        if (ids.size() > 0) {
            Set<ObjectId> experimentIds = ids.stream().map(ObjectId::new).collect(Collectors.toSet());
            Page<ExperimentMetaData> page = experimentMetaDataRepository.findAllInExperiments(experimentIds, pageable);
            return page;
        } else {
            return Page.empty();
        }
    }

    /**
     * Get one experimentMetaData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ExperimentMetaData> findOne(String id) {
        log.debug("Request to get ExperimentMetaData : {}", id);
        return findOneByRole(id);
    }

    /**
     * Delete the experimentMetaData by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        if (!canUpdate(id)) {
            throw new NotAuthorizedException();
        }
        log.debug("Request to delete ExperimentMetaData : {}", id);
        ExperimentMetaData experimentMetaData = experimentMetaDataRepository.findById(id).get();
        experimentMetaDataRepository.delete(experimentMetaData);
        experimentDataRepository.deleteAllByExperimentMetaData(id);
        experimentService.removeExperimentMetaData(experimentMetaData.getExperiment().getId(), experimentMetaData);
    }

    public void addExperimentData(String experimentId, ExperimentData experimentData) {
        ExperimentMetaData experimentMetaData = experimentMetaDataRepository.findById(experimentId).get();
        experimentMetaData.addExperimentData(experimentData);
        experimentMetaDataRepository.save(experimentMetaData);
    }
    public void removeExperimentData(String experimentId, ExperimentData experimentData) {
        ExperimentMetaData experimentMetaData = experimentMetaDataRepository.findById(experimentId).get();
        experimentMetaData.removeExperimentData(experimentData);
        experimentMetaDataRepository.save(experimentMetaData);
    }
    public boolean canUpdate(String id) {
        return canUpdate(experimentMetaDataRepository.findById(id));
    }

    public boolean canUpdate(ExperimentMetaData experimentMetaData) {
        return canUpdate(Optional.of(experimentMetaData));
    }

    public boolean canUpdate(Optional<ExperimentMetaData> experimentMetaData) {
        if (experimentMetaData.isPresent()) {
            return experimentService.canUpdate(experimentMetaData.get().getExperiment().getId());
        }
        return false;
    }

    public ExperimentDataLog insertLog(ExperimentDataLog dataLog) {
        log.debug("Request to insert ExperimentDataLog : {}", dataLog);
        return logRepository.save(dataLog);
    }

    private Optional<ExperimentMetaData> findOneByRole(String id) {
        Optional<ExperimentMetaData> metaData = experimentMetaDataRepository.findById(id);
        if (metaData.isPresent()) {
            Optional<Experiment> experiment = experimentService.findOne(metaData.get().getExperiment().getId());
            if (!experiment.isPresent()) {
                return Optional.empty();
            }
            metaData.get().setLogs(logRepository.findAllByExperimentDataId(metaData.get().getId()));
        }
        return metaData;
    }

    public byte[] getZipFile(String id) {
        Set<ExperimentData> alldata = experimentDataRepository.findAllByExperimentMetaData(id);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        String index = "id,participants,created_date,created_by\n";
        try {
            for (ExperimentData expData : alldata) {
                String filename = expData.getId().concat(".").concat(expData.getDataContentType().split("/")[1]);
                ZipEntry entry = new ZipEntry(filename);
                //byte[] data = Base64.decode(new String(expData.getData()));
                entry.setSize(expData.getData().length);
                zos.putNextEntry(entry);
                zos.write(expData.getData());
                zos.closeEntry();
                index +=
                    expData
                        .getId()
                        .concat(",")
                        .concat(expData.getParticipants())
                        .concat(",")
                        .concat(expData.getCreatedDate().toString())
                        .concat(",")
                        .concat(expData.getCreatedBy())
                        .concat("\n");
            }
            ZipEntry entryRes = new ZipEntry("index.csv");
            zos.putNextEntry(entryRes);
            zos.write(index.getBytes(StandardCharsets.UTF_8));
            zos.closeEntry();
            zos.close();
        } catch (IOException e) {
            log.error("Error creating zip file");
        }
        insertLog(new ExperimentDataLog(id, LogAction.DOWNLOAD_DATA));
        return baos.toByteArray();
    }

    public byte[] getPluginView(String id, String className) {
        Optional<ExperimentMetaData> metaData = findOne(id);
        PluginViewer plugin = pluginLoader.getViewer(className);
        if (plugin != null && metaData.isPresent()) {
            ExperimentMetaDataDTO dto = experimentMetaDataMapper.metaDatatoDTO(metaData.get());
            return plugin.view(dto);
        }
        return null;
    }
}

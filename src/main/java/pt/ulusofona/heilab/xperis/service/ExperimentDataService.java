package pt.ulusofona.heilab.xperis.service;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import pt.ulusofona.heilab.xperis.domain.ExperimentData;
import pt.ulusofona.heilab.xperis.domain.ExperimentDataLog;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.domain.enumeration.LogAction;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataLogRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataRepository;
import pt.ulusofona.heilab.xperis.service.exception.NotAuthorizedException;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ExperimentData}.
 */
@Service
public class ExperimentDataService {

    private final Logger log = LoggerFactory.getLogger(ExperimentDataService.class);

    private final ExperimentDataRepository experimentDataRepository;
    private final ExperimentDataLogRepository logRepository;
    private final ExperimentMetaDataService experimentMetaDataService;

    public ExperimentDataService(
        ExperimentDataRepository experimentDataRepository,
        ExperimentDataLogRepository logRepository,
        ExperimentMetaDataService experimentMetaDataService
    ) {
        this.experimentDataRepository = experimentDataRepository;
        this.logRepository = logRepository;
        this.experimentMetaDataService = experimentMetaDataService;
    }

    /**
     * Save a experimentData.
     *
     * @param experimentData the entity to save.
     * @return the persisted entity.
     */
    public ExperimentData save(ExperimentData experimentData) {
        if (!canUpdate(experimentData)) {
            throw new NotAuthorizedException();
        }
        log.debug("Request to save ExperimentData : {}", experimentData);
        if (!canView(experimentData.getExperimentMetaData().getId())) {
            return null;
        }
        ExperimentData newData = experimentDataRepository.save(experimentData);
        experimentMetaDataService.addExperimentData(experimentData.getExperimentMetaData().getId(), newData);
        return newData;
    }

    public boolean canView(String experimentMetaDataId) {
        Optional<ExperimentMetaData> experimentMetaData = experimentMetaDataService.findOne(experimentMetaDataId);
        return experimentMetaData.isPresent();
    }

    /**
     * Partially update a experimentData.
     *
     * @param experimentData the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ExperimentData> partialUpdate(ExperimentData experimentData) {
        log.debug("Request to partially update ExperimentData : {}", experimentData);

        return experimentDataRepository
            .findById(experimentData.getId())
            .map(
                existingExperimentData -> {
                    if (!canUpdate(existingExperimentData)) {
                        throw new NotAuthorizedException();
                    }
                    if (!canView(existingExperimentData.getExperimentMetaData().getId())) {
                        return null;
                    }
                    if (!ObjectUtils.nullSafeEquals(experimentData.getData(), existingExperimentData.getData())) {
                        insertLog(new ExperimentDataLog(existingExperimentData.getId(), LogAction.UPDATE_DATA));
                    } else {
                        insertLog(new ExperimentDataLog(existingExperimentData.getId(), LogAction.UPDATE));
                    }

                    if (experimentData.getParticipants() != null) {
                        existingExperimentData.setParticipants(experimentData.getParticipants());
                    }
                    if (experimentData.getData() != null) {
                        existingExperimentData.setData(experimentData.getData());
                    }
                    if (experimentData.getDataContentType() != null) {
                        existingExperimentData.setDataContentType(experimentData.getDataContentType());
                    }
                    return existingExperimentData;
                }
            )
            .map(experimentDataRepository::save);
    }

    /**
     * Get all the experimentData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<ExperimentData> findAll(Pageable pageable) {
        log.debug("Request to get all ExperimentData");

        Page<ExperimentMetaData> metaDatas = experimentMetaDataService.findAll(Pageable.unpaged());
        if (metaDatas.getSize() > 0) {
            Set<ObjectId> metadatasIds = metaDatas.stream().map(ExperimentMetaData::getId).map(ObjectId::new).collect(Collectors.toSet());
            return experimentDataRepository.findAllInExperimentMetaDatas(metadatasIds, pageable);
        } else {
            return Page.empty();
        }
    }

    /**
     * Get one experimentData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ExperimentData> findOne(String id) {
        log.debug("Request to get ExperimentData : {}", id);
        return findOneByRole(id);
    }

    /**
     * Delete the experimentData by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        if (!canUpdate(id)) {
            throw new NotAuthorizedException();
        }
        log.debug("Request to delete ExperimentData : {}", id);
        ExperimentData experimentData = experimentDataRepository.findById(id).get();
        experimentDataRepository.delete(experimentData);
        experimentMetaDataService.removeExperimentData(experimentData.getExperimentMetaData().getId(), experimentData);
    }

    public boolean canUpdate(String id) {
        return canUpdate(experimentDataRepository.findById(id));
    }

    private boolean canUpdate(ExperimentData experimentData) {
        return canUpdate(Optional.of(experimentData));
    }

    private boolean canUpdate(Optional<ExperimentData> experimentData) {
        return experimentData.filter(data -> experimentMetaDataService.canUpdate(data.getExperimentMetaData().getId())).isPresent();
    }

    public ExperimentDataLog insertLog(ExperimentDataLog dataLog) {
        log.debug("Request to insert ExperimentDataLog : {}", dataLog);
        return logRepository.save(dataLog);
    }

    private Optional<ExperimentData> findOneByRole(String id) {
        Optional<ExperimentData> data = experimentDataRepository.findById(id);
        if (data.isPresent()) {
            Optional<ExperimentMetaData> experimentMetaData = experimentMetaDataService.findOne(data.get().getExperimentMetaData().getId());
            if (experimentMetaData.isEmpty()) {
                return Optional.empty();
            }
            data.get().setLogs(logRepository.findAllByExperimentDataId(data.get().getId()));
        }
        return data;
    }
}

package pt.ulusofona.heilab.xperis.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.DataTemplate;
import pt.ulusofona.heilab.xperis.repository.DataTemplateRepository;

/**
 * Service Implementation for managing {@link DataTemplate}.
 */
@Service
public class DataTemplateService {

    private final Logger log = LoggerFactory.getLogger(DataTemplateService.class);

    private final DataTemplateRepository dataTemplateRepository;

    public DataTemplateService(DataTemplateRepository dataTemplateRepository) {
        this.dataTemplateRepository = dataTemplateRepository;
    }

    /**
     * Save a dataTemplate.
     *
     * @param dataTemplate the entity to save.
     * @return the persisted entity.
     */
    public DataTemplate save(DataTemplate dataTemplate) {
        log.debug("Request to save DataTemplate : {}", dataTemplate);
        return dataTemplateRepository.save(dataTemplate);
    }

    /**
     * Partially update a dataTemplate.
     *
     * @param dataTemplate the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DataTemplate> partialUpdate(DataTemplate dataTemplate) {
        log.debug("Request to partially update DataTemplate : {}", dataTemplate);

        return dataTemplateRepository
            .findById(dataTemplate.getId())
            .map(
                existingDataTemplate -> {
                    if (dataTemplate.getName() != null) {
                        existingDataTemplate.setName(dataTemplate.getName());
                    }
                    if (dataTemplate.getDataStructure() != null) {
                        existingDataTemplate.setDataStructure(dataTemplate.getDataStructure());
                    }
                    if (dataTemplate.getViewerClasses() != null) {
                        existingDataTemplate.setViewerClasses(dataTemplate.getViewerClasses());
                    }
                    if (dataTemplate.getDataMimeType() != null) {
                        existingDataTemplate.setDataMimeType(dataTemplate.getDataMimeType());
                    }
                    return existingDataTemplate;
                }
            )
            .map(dataTemplateRepository::save);
    }

    /**
     * Get all the dataTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<DataTemplate> findAll(Pageable pageable) {
        log.debug("Request to get all DataTemplates");
        return dataTemplateRepository.findAll(pageable);
    }

    /**
     * Get one dataTemplate by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<DataTemplate> findOne(String id) {
        log.debug("Request to get DataTemplate : {}", id);
        return dataTemplateRepository.findById(id);
    }

    /**
     * Delete the dataTemplate by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete DataTemplate : {}", id);
        dataTemplateRepository.deleteById(id);
    }
}

package pt.ulusofona.heilab.xperis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pt.ulusofona.heilab.xperis.domain.Plugin;
import pt.ulusofona.heilab.xperis.service.spi.PluginViewer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

@Component
public class PluginLoader {

    private final Logger log = LoggerFactory.getLogger(PluginLoader.class);

    private Map<String, Plugin> loadedPlugins;

    private final String FILE_EXTENSION = ".jar";

    private final PluginService pluginService;
    private final Environment env;

    public PluginLoader(PluginService pluginService, Environment env) {
        this.pluginService = pluginService;
        this.env = env;
        load();
    }

    public void load() {
        loadedPlugins = new HashMap<>();

        ServiceLoader<PluginViewer> systemServiceLoader = ServiceLoader.load(PluginViewer.class);
        systemServiceLoader.iterator().forEachRemaining(viewer -> loadedPlugins.put(viewer.getClass().getSimpleName(), getPlugin(viewer)));
        log.info("Plugins loaded from SystemLoader: {}", loadedPlugins.size());

        Path[] jarPaths = listJarFiles();
        URLClassLoader childClassLoader = new URLClassLoader(toURL(jarPaths), Thread.currentThread().getContextClassLoader());
        ServiceLoader<PluginViewer> jarServiceLoader = ServiceLoader.load(PluginViewer.class, childClassLoader);

        int i = 0;
        for (PluginViewer viewer : jarServiceLoader) {
            Plugin plugin = getPlugin(viewer);
            plugin.setFileName(jarPaths[i].getFileName().toString());
            loadedPlugins.put(plugin.getName(), plugin);
            i++;
        }
        log.info("Plugins loaded from JarFolder: {}", i);

        this.updateRepository();
    }

    private Plugin getPlugin(PluginViewer viewer) {
        Plugin plugin = new Plugin();
        plugin.setName(viewer.getClass().getSimpleName());
        plugin.setClassName(viewer.getClass().getCanonicalName());
        plugin.setViewer(viewer);
        return plugin;
    }

    public Set<String> getNames() {
        return loadedPlugins.keySet();
    }

    public Plugin getPlugin(String className) {
        return loadedPlugins.get(className);
    }

    public PluginViewer getViewer(String className) {
        Plugin info = getPlugin(className);
        if (info != null) return info.getViewer();
        return null;
    }

    public void addPlugin(Plugin plugin) {
        try {
            File pluginFolder = getPluginFolder();
            Path path = new File(pluginFolder, plugin.getFileName()).toPath();
            if (Arrays.asList(pluginFolder.list()).contains(plugin.getFileName())) {
                log.info("File already exists: {}", path.toString());
                return;
            }
            Files.write(path, plugin.getData());
            log.info("File saved: {}", path.toString());
            load();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    public void deletePlugin(Plugin plugin) {
        try {
            File pluginFolder = getPluginFolder();
            Path path = new File(pluginFolder, plugin.getFileName()).toPath();
            if (!Arrays.asList(pluginFolder.list()).contains(plugin.getFileName())) {
                log.info("File does not exists: {}", path.toString());
                return;
            }
            Files.delete(path);
            log.info("File deleted: {}", path.toString());
            load();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private File getPluginFolder(){
        String folder = env.getProperty("xperis.conf-folder");
        log.info("Config Folder Configured (ENV): {}", folder);
        File configFolder = new File(folder);
        if (!configFolder.exists()) configFolder.mkdir();

        File pluginFolder = new File(folder + "/plugins");
        if (!pluginFolder.exists()) pluginFolder.mkdir();
        return pluginFolder;
    }

    private Path[] listJarFiles() {
        try {
            File pluginFolder = getPluginFolder();

            Stream<Path> stream = Files.walk(pluginFolder.toPath(), 1);
            return stream
                .filter(file -> !Files.isDirectory(file))
                .filter(file -> file.getFileName().toString().endsWith(FILE_EXTENSION))
                .peek(file -> log.info("File read: {}", file.toString()))
                .toArray(Path[]::new);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private URL[] toURL(Path[] paths) {
        return Arrays
            .stream(paths)
            .map(this::toURL)
            .toArray(URL[]::new);
    }

    private URL toURL(Path path) {
        try {
            return path.toUri().toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Update Plugins Repository with memory loaded plugins.
     */
    public void updateRepository() {
        List<Plugin> dbPlugins = pluginService.findAll();
        log.info("Plugins loaded from Database: {}", dbPlugins.size());

        Set<String> loaded = loadedPlugins.keySet();

        for (Plugin plugin : dbPlugins) {
            boolean found = loaded.stream().anyMatch(name -> plugin.getName().equals(name));
            if (found) {
                plugin.setValid(true);
                log.info("Plugin valid: {}", plugin.getName());
            } else {
                plugin.setValid(false);
                plugin.setActivated(false);
                log.warn("Plugin invalid: {}", plugin.getName());
            }
            pluginService.save(plugin);
        }

        for (String name : loaded) {
            boolean found = dbPlugins.stream().anyMatch(plugin -> plugin.getName().equals(name));
            if (!found) {
                Plugin plugin = loadedPlugins.get(name);
                plugin.setValid(true);
                plugin.setActivated(true);
                pluginService.save(plugin, plugin.getViewer().getMimeTypeIn());
                log.info("Plugin inserted: {}", plugin.getName());
            }
        }
    }
}

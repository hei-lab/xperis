package pt.ulusofona.heilab.xperis.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.Institution;
import pt.ulusofona.heilab.xperis.repository.InstitutionRepository;

/**
 * Service Implementation for managing {@link Institution}.
 */
@Service
public class InstitutionService {

    private final Logger log = LoggerFactory.getLogger(InstitutionService.class);

    private final InstitutionRepository institutionRepository;

    public InstitutionService(InstitutionRepository institutionRepository) {
        this.institutionRepository = institutionRepository;
    }

    /**
     * Save a institution.
     *
     * @param institution the entity to save.
     * @return the persisted entity.
     */
    public Institution save(Institution institution) {
        log.debug("Request to save Institution : {}", institution);
        return institutionRepository.save(institution);
    }

    /**
     * Partially update a institution.
     *
     * @param institution the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Institution> partialUpdate(Institution institution) {
        log.debug("Request to partially update Institution : {}", institution);

        return institutionRepository
            .findById(institution.getId())
            .map(
                existingInstitution -> {
                    if (institution.getName() != null) {
                        existingInstitution.setName(institution.getName());
                    }

                    return existingInstitution;
                }
            )
            .map(institutionRepository::save);
    }

    /**
     * Get all the institutions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<Institution> findAll(Pageable pageable) {
        log.debug("Request to get all Institutions");
        return institutionRepository.findAll(pageable);
    }

    /**
     * Get all the institutions with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<Institution> findAllWithEagerRelationships(Pageable pageable) {
        return institutionRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one institution by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Institution> findOne(String id) {
        log.debug("Request to get Institution : {}", id);
        return institutionRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the institution by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete Institution : {}", id);
        institutionRepository.deleteById(id);
    }
}

package pt.ulusofona.heilab.xperis.security.apikey;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.security.DomainUserDetailsService;

@Service
public class APIKeyManager implements AuthenticationManager {

    private final DomainUserDetailsService userDetailsService;

    public APIKeyManager(DomainUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Object principal = authentication.getPrincipal();
        if (!(principal instanceof UserDetails)) {
            throw new BadCredentialsException("The API key was not found or not the expected value.");
        }
        authentication.setAuthenticated(true);
        return authentication;
    }

    public Object getUser(String header) {
        return userDetailsService.loadUserById(header);
    }
}

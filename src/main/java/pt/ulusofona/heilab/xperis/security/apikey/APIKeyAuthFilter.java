package pt.ulusofona.heilab.xperis.security.apikey;

import io.jsonwebtoken.Claims;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class APIKeyAuthFilter extends AbstractPreAuthenticatedProcessingFilter {

    private final String API_KEY_HEADER = "user-api-key";
    private final String API_KEY_VALUE = "XPERISTESTAPIKEY";

    private final APIKeyManager apiKeyManager;

    public APIKeyAuthFilter(APIKeyManager apiKeyManager) {
        this.apiKeyManager = apiKeyManager;
        this.setAuthenticationManager(apiKeyManager);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String apikey = request.getHeader(API_KEY_HEADER);
        if (StringUtils.hasText(apikey)) {
            try {
                UserDetails user = (UserDetails) apiKeyManager.getUser(apikey);
                if (user != null) {
                    Authentication authentication = new UsernamePasswordAuthenticationToken(
                        user,
                        user.getUsername(),
                        user.getAuthorities()
                    );
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            } catch (UsernameNotFoundException e) {}
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return apiKeyManager.getUser(request.getHeader(API_KEY_HEADER));
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return "N/A";
    }
}

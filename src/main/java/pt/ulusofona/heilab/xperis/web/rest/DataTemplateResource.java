package pt.ulusofona.heilab.xperis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ulusofona.heilab.xperis.domain.DataTemplate;
import pt.ulusofona.heilab.xperis.repository.DataTemplateRepository;
import pt.ulusofona.heilab.xperis.service.DataTemplateService;
import pt.ulusofona.heilab.xperis.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link pt.ulusofona.heilab.xperis.domain.DataTemplate}.
 */
@RestController
@RequestMapping("/api")
public class DataTemplateResource {

    private final Logger log = LoggerFactory.getLogger(DataTemplateResource.class);

    private static final String ENTITY_NAME = "dataTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DataTemplateService dataTemplateService;

    private final DataTemplateRepository dataTemplateRepository;

    public DataTemplateResource(DataTemplateService dataTemplateService, DataTemplateRepository dataTemplateRepository) {
        this.dataTemplateService = dataTemplateService;
        this.dataTemplateRepository = dataTemplateRepository;
    }

    /**
     * {@code POST  /data-templates} : Create a new dataTemplate.
     *
     * @param dataTemplate the dataTemplate to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dataTemplate, or with status {@code 400 (Bad Request)} if the dataTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/data-templates")
    public ResponseEntity<DataTemplate> createDataTemplate(@Valid @RequestBody DataTemplate dataTemplate) throws URISyntaxException {
        log.debug("REST request to save DataTemplate : {}", dataTemplate);
        if (dataTemplate.getId() != null) {
            throw new BadRequestAlertException("A new dataTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DataTemplate result = dataTemplateService.save(dataTemplate);
        return ResponseEntity
            .created(new URI("/api/data-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /data-templates/:id} : Updates an existing dataTemplate.
     *
     * @param id the id of the dataTemplate to save.
     * @param dataTemplate the dataTemplate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dataTemplate,
     * or with status {@code 400 (Bad Request)} if the dataTemplate is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dataTemplate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/data-templates/{id}")
    public ResponseEntity<DataTemplate> updateDataTemplate(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody DataTemplate dataTemplate
    ) throws URISyntaxException {
        log.debug("REST request to update DataTemplate : {}, {}", id, dataTemplate);
        if (dataTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dataTemplate.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dataTemplateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DataTemplate result = dataTemplateService.save(dataTemplate);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dataTemplate.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /data-templates/:id} : Partial updates given fields of an existing dataTemplate, field will ignore if it is null
     *
     * @param id the id of the dataTemplate to save.
     * @param dataTemplate the dataTemplate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dataTemplate,
     * or with status {@code 400 (Bad Request)} if the dataTemplate is not valid,
     * or with status {@code 404 (Not Found)} if the dataTemplate is not found,
     * or with status {@code 500 (Internal Server Error)} if the dataTemplate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/data-templates/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DataTemplate> partialUpdateDataTemplate(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody DataTemplate dataTemplate
    ) throws URISyntaxException {
        log.debug("REST request to partial update DataTemplate partially : {}, {}", id, dataTemplate);
        if (dataTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dataTemplate.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dataTemplateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DataTemplate> result = dataTemplateService.partialUpdate(dataTemplate);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dataTemplate.getId())
        );
    }

    /**
     * {@code GET  /data-templates} : get all the dataTemplates.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dataTemplates in body.
     */
    @GetMapping("/data-templates")
    public ResponseEntity<List<DataTemplate>> getAllDataTemplates(Pageable pageable) {
        log.debug("REST request to get a page of DataTemplates");
        Page<DataTemplate> page = dataTemplateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /data-templates/:id} : get the "id" dataTemplate.
     *
     * @param id the id of the dataTemplate to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dataTemplate, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/data-templates/{id}")
    public ResponseEntity<DataTemplate> getDataTemplate(@PathVariable String id) {
        log.debug("REST request to get DataTemplate : {}", id);
        Optional<DataTemplate> dataTemplate = dataTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dataTemplate);
    }

    /**
     * {@code DELETE  /data-templates/:id} : delete the "id" dataTemplate.
     *
     * @param id the id of the dataTemplate to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/data-templates/{id}")
    public ResponseEntity<Void> deleteDataTemplate(@PathVariable String id) {
        log.debug("REST request to delete DataTemplate : {}", id);
        dataTemplateService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}

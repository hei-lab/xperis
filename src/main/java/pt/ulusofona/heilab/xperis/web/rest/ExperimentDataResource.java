package pt.ulusofona.heilab.xperis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ulusofona.heilab.xperis.domain.ExperimentData;
import pt.ulusofona.heilab.xperis.domain.ExperimentDataLog;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataRepository;
import pt.ulusofona.heilab.xperis.security.AuthoritiesConstants;
import pt.ulusofona.heilab.xperis.service.ExperimentDataService;
import pt.ulusofona.heilab.xperis.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link pt.ulusofona.heilab.xperis.domain.ExperimentData}.
 */
@RestController
@RequestMapping("/api")
public class ExperimentDataResource {

    private final Logger log = LoggerFactory.getLogger(ExperimentDataResource.class);

    private static final String ENTITY_NAME = "experimentData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExperimentDataService experimentDataService;

    private final ExperimentDataRepository experimentDataRepository;

    public ExperimentDataResource(ExperimentDataService experimentDataService, ExperimentDataRepository experimentDataRepository) {
        this.experimentDataService = experimentDataService;
        this.experimentDataRepository = experimentDataRepository;
    }

    /**
     * {@code POST  /experiment-data} : Create a new experimentData.
     *
     * @param experimentData the experimentData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new experimentData, or with status {@code 400 (Bad Request)} if the experimentData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/experiment-data")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<ExperimentData> createExperimentData(@Valid @RequestBody ExperimentData experimentData)
        throws URISyntaxException {
        log.debug("REST request to save ExperimentData : {}", experimentData);
        if (experimentData.getId() != null) {
            throw new BadRequestAlertException("A new experimentData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExperimentData result = experimentDataService.save(experimentData);
        return ResponseEntity
            .created(new URI("/api/experiment-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/experiment-data-log")
    public ResponseEntity<ExperimentDataLog> insertExperimentDataLog(@Valid @RequestBody ExperimentDataLog experimentDataLog)
        throws URISyntaxException {
        log.debug("REST request to save ExperimentDataLog : {}", experimentDataLog);
        if (experimentDataLog.getId() != null) {
            throw new BadRequestAlertException("A new experimentDataLog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExperimentDataLog result = experimentDataService.insertLog(experimentDataLog);
        return ResponseEntity.created(new URI("/api/experiment-data-log/" + result.getId())).body(result);
    }

    /**
     * {@code PUT  /experiment-data/:id} : Updates an existing experimentData.
     *
     * @param id the id of the experimentData to save.
     * @param experimentData the experimentData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated experimentData,
     * or with status {@code 400 (Bad Request)} if the experimentData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the experimentData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/experiment-data/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<ExperimentData> updateExperimentData(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody ExperimentData experimentData
    ) throws URISyntaxException {
        log.debug("REST request to update ExperimentData : {}, {}", id, experimentData);
        if (experimentData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, experimentData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!experimentDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExperimentData result = experimentDataService.save(experimentData);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, experimentData.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /experiment-data/:id} : Partial updates given fields of an existing experimentData, field will ignore if it is null
     *
     * @param id the id of the experimentData to save.
     * @param experimentData the experimentData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated experimentData,
     * or with status {@code 400 (Bad Request)} if the experimentData is not valid,
     * or with status {@code 404 (Not Found)} if the experimentData is not found,
     * or with status {@code 500 (Internal Server Error)} if the experimentData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/experiment-data/{id}", consumes = "application/merge-patch+json")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<ExperimentData> partialUpdateExperimentData(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody ExperimentData experimentData
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExperimentData partially : {}, {}", id, experimentData);
        if (experimentData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, experimentData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!experimentDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExperimentData> result = experimentDataService.partialUpdate(experimentData);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, experimentData.getId())
        );
    }

    /**
     * {@code GET  /experiment-data} : get all the experimentData.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of experimentData in body.
     */
    @GetMapping("/experiment-data")
    public ResponseEntity<List<ExperimentData>> getAllExperimentData(Pageable pageable) {
        log.debug("REST request to get a page of ExperimentData");
        Page<ExperimentData> page = experimentDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /experiment-data/:id} : get the "id" experimentData.
     *
     * @param id the id of the experimentData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the experimentData, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/experiment-data/{id}")
    public ResponseEntity<ExperimentData> getExperimentData(@PathVariable String id) {
        log.debug("REST request to get ExperimentData : {}", id);
        Optional<ExperimentData> experimentData = experimentDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(experimentData);
    }

    /**
     * {@code DELETE  /experiment-data/:id} : delete the "id" experimentData.
     *
     * @param id the id of the experimentData to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/experiment-data/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<Void> deleteExperimentData(@PathVariable String id) {
        log.debug("REST request to delete ExperimentData : {}", id);
        experimentDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}

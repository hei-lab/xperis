/**
 * View Models used by Spring MVC REST controllers.
 */
package pt.ulusofona.heilab.xperis.web.rest.vm;

package pt.ulusofona.heilab.xperis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.repository.ExperimentRepository;
import pt.ulusofona.heilab.xperis.security.AuthoritiesConstants;
import pt.ulusofona.heilab.xperis.service.ExperimentService;
import pt.ulusofona.heilab.xperis.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link pt.ulusofona.heilab.xperis.domain.Experiment}.
 */
@RestController
@RequestMapping("/api")
public class ExperimentResource {

    private final Logger log = LoggerFactory.getLogger(ExperimentResource.class);

    private static final String ENTITY_NAME = "experiment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExperimentService experimentService;

    private final ExperimentRepository experimentRepository;

    public ExperimentResource(ExperimentService experimentService, ExperimentRepository experimentRepository) {
        this.experimentService = experimentService;
        this.experimentRepository = experimentRepository;
    }

    /**
     * {@code POST  /experiments} : Create a new experiment.
     *
     * @param experiment the experiment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new experiment, or with status {@code 400 (Bad Request)} if the experiment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/experiments")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<Experiment> createExperiment(@Valid @RequestBody Experiment experiment) throws URISyntaxException {
        log.debug("REST request to save Experiment : {}", experiment);
        if (experiment.getId() != null) {
            throw new BadRequestAlertException("A new experiment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Experiment result = experimentService.save(experiment);
        return ResponseEntity
            .created(new URI("/api/experiments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /experiments/:id} : Updates an existing experiment.
     *
     * @param id the id of the experiment to save.
     * @param experiment the experiment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated experiment,
     * or with status {@code 400 (Bad Request)} if the experiment is not valid,
     * or with status {@code 500 (Internal Server Error)} if the experiment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/experiments/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<Experiment> updateExperiment(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody Experiment experiment
    ) throws URISyntaxException {
        log.debug("REST request to update Experiment : {}, {}", id, experiment);
        if (experiment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, experiment.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!experimentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Experiment result = experimentService.save(experiment);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, experiment.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /experiments/:id} : Partial updates given fields of an existing experiment, field will ignore if it is null
     *
     * @param id the id of the experiment to save.
     * @param experiment the experiment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated experiment,
     * or with status {@code 400 (Bad Request)} if the experiment is not valid,
     * or with status {@code 404 (Not Found)} if the experiment is not found,
     * or with status {@code 500 (Internal Server Error)} if the experiment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/experiments/{id}", consumes = "application/merge-patch+json")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<Experiment> partialUpdateExperiment(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody Experiment experiment
    ) throws URISyntaxException {
        log.debug("REST request to partial update Experiment partially : {}, {}", id, experiment);
        if (experiment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, experiment.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!experimentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Experiment> result = experimentService.partialUpdate(experiment);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, experiment.getId())
        );
    }

    /**
     * {@code GET  /experiments} : get all the experiments.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of experiments in body.
     */
    @GetMapping("/experiments")
    public ResponseEntity<List<Experiment>> getAllExperiments(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Experiments");
        Page<Experiment> page;
        if (eagerload) {
            page = experimentService.findAllWithEagerRelationships(pageable);
        } else {
            page = experimentService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /experiments/:id} : get the "id" experiment.
     *
     * @param id the id of the experiment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the experiment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/experiments/{id}")
    public ResponseEntity<Experiment> getExperiment(@PathVariable String id) {
        log.debug("REST request to get Experiment : {}", id);
        Optional<Experiment> experiment = experimentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(experiment);
    }

    /**
     * {@code DELETE  /experiments/:id} : delete the "id" experiment.
     *
     * @param id the id of the experiment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/experiments/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<Void> deleteExperiment(@PathVariable String id) {
        log.debug("REST request to delete Experiment : {}", id);
        experimentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}

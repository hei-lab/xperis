import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IExperiment } from '../experiment.model';
import { ExperimentService } from '../service/experiment.service';

@Component({
  templateUrl: './experiment-delete-dialog.component.html',
})
export class ExperimentDeleteDialogComponent {
  experiment?: IExperiment;

  constructor(protected experimentService: ExperimentService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.experimentService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IExperiment, Experiment } from '../experiment.model';
import { ExperimentService } from '../service/experiment.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';

@Component({
  selector: 'jhi-experiment-update',
  templateUrl: './experiment-update.component.html',
})
export class ExperimentUpdateComponent implements OnInit {
  isSaving = false;

  account: Account | null = null;
  authSubscription?: Subscription;

  usersSharedCollection: IUser[] = [];
  membersCollection: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    startDate: [],
    finishCaptureDate: [],
    finishDate: [],
    status: [],
    members: [],
    assistants: [],
    createdBy: [],
  });

  constructor(
    protected experimentService: ExperimentService,
    protected userService: UserService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.activatedRoute.data.subscribe(({ experiment }) => {
      if (experiment.id === undefined) {
        const today = dayjs().startOf('day');
        experiment.startDate = today;
        experiment.finishCaptureDate = today;
        experiment.finishDate = today;
      }

      this.updateForm(experiment);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const experiment = this.createFromForm();
    if (experiment.id !== undefined) {
      this.subscribeToSaveResponse(this.experimentService.partialUpdate(experiment));
    } else {
      this.subscribeToSaveResponse(this.experimentService.create(experiment));
    }
  }

  trackUserById(index: number, item: IUser): string {
    return item.id!;
  }

  getSelectedUser(option: IUser, selectedVals?: IUser[]): IUser {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExperiment>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(experiment: IExperiment): void {
    this.editForm.patchValue({
      id: experiment.id,
      name: experiment.name,
      description: experiment.description,
      startDate: experiment.startDate ? experiment.startDate.format(DATE_TIME_FORMAT) : null,
      finishCaptureDate: experiment.finishCaptureDate ? experiment.finishCaptureDate.format(DATE_TIME_FORMAT) : null,
      finishDate: experiment.finishDate ? experiment.finishDate.format(DATE_TIME_FORMAT) : null,
      status: experiment.status,
      members: experiment.members,
      assistants: experiment.assistants,
      createdBy: experiment.createdBy,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(
      this.usersSharedCollection,
      ...(experiment.assistants ?? [])
    );
    this.membersCollection = this.userService.addUserToCollectionIfMissing(this.membersCollection, ...(experiment.members ?? []));
  }

  protected loadRelationshipsOptions(): void {
    const owner = this.editForm.get(['createdBy'])!.value || this.account!.login;
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(
        map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, ...(this.editForm.get('assistants')!.value ?? [])))
      )
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users.filter(user => user.login !== owner)));
    this.userService
      .queryAuth('ROLE_MASTER')
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, ...(this.editForm.get('members')!.value ?? []))))
      .subscribe((users: IUser[]) => (this.membersCollection = users.filter(user => user.login !== owner)));
  }

  protected createFromForm(): IExperiment {
    return {
      ...new Experiment(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      startDate: this.editForm.get(['startDate'])!.value ? dayjs(this.editForm.get(['startDate'])!.value, DATE_TIME_FORMAT) : undefined,
      finishCaptureDate: this.editForm.get(['finishCaptureDate'])!.value
        ? dayjs(this.editForm.get(['finishCaptureDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      finishDate: this.editForm.get(['finishDate'])!.value ? dayjs(this.editForm.get(['finishDate'])!.value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status'])!.value,
      members: this.editForm.get(['members'])!.value,
      assistants: this.editForm.get(['assistants'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
    };
  }
}

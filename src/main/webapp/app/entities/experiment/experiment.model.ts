import * as dayjs from 'dayjs';
import { IExperimentMetaData } from 'app/entities/experiment-meta-data/experiment-meta-data.model';
import { IUser } from 'app/entities/user/user.model';
import { ExperimentStatus } from 'app/entities/enumerations/experiment-status.model';

export interface IExperiment {
  id?: string;
  name?: string;
  description?: string | null;
  startDate?: dayjs.Dayjs | null;
  finishCaptureDate?: dayjs.Dayjs | null;
  finishDate?: dayjs.Dayjs | null;
  status?: ExperimentStatus | null;
  experimentMetaData?: IExperimentMetaData[] | null;
  members?: IUser[] | null;
  assistants?: IUser[] | null;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Experiment implements IExperiment {
  constructor(
    public id?: string,
    public name?: string,
    public description?: string | null,
    public startDate?: dayjs.Dayjs | null,
    public finishCaptureDate?: dayjs.Dayjs | null,
    public finishDate?: dayjs.Dayjs | null,
    public status?: ExperimentStatus | null,
    public experimentMetaData?: IExperimentMetaData[] | null,
    public members?: IUser[] | null,
    public assistants?: IUser[] | null,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {}
}

export function getExperimentIdentifier(experiment: IExperiment): string | undefined {
  return experiment.id;
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExperiment } from '../experiment.model';

@Component({
  selector: 'jhi-experiment-detail',
  templateUrl: './experiment-detail.component.html',
})
export class ExperimentDetailComponent implements OnInit {
  experiment: IExperiment | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ experiment }) => {
      this.experiment = experiment;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

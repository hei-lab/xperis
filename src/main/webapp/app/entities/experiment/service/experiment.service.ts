import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IExperiment, getExperimentIdentifier } from '../experiment.model';

export type EntityResponseType = HttpResponse<IExperiment>;
export type EntityArrayResponseType = HttpResponse<IExperiment[]>;

@Injectable({ providedIn: 'root' })
export class ExperimentService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/experiments');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(experiment: IExperiment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(experiment);
    return this.http
      .post<IExperiment>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(experiment: IExperiment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(experiment);
    return this.http
      .put<IExperiment>(`${this.resourceUrl}/${getExperimentIdentifier(experiment) as string}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(experiment: IExperiment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(experiment);
    return this.http
      .patch<IExperiment>(`${this.resourceUrl}/${getExperimentIdentifier(experiment) as string}`, copy, {
        observe: 'response',
        headers: { 'Content-Type': 'application/merge-patch+json' },
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IExperiment>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExperiment[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addExperimentToCollectionIfMissing(
    experimentCollection: IExperiment[],
    ...experimentsToCheck: (IExperiment | null | undefined)[]
  ): IExperiment[] {
    const experiments: IExperiment[] = experimentsToCheck.filter(isPresent);
    if (experiments.length > 0) {
      const experimentCollectionIdentifiers = experimentCollection.map(experimentItem => getExperimentIdentifier(experimentItem)!);
      const experimentsToAdd = experiments.filter(experimentItem => {
        const experimentIdentifier = getExperimentIdentifier(experimentItem);
        if (experimentIdentifier == null || experimentCollectionIdentifiers.includes(experimentIdentifier)) {
          return false;
        }
        experimentCollectionIdentifiers.push(experimentIdentifier);
        return true;
      });
      return [...experimentsToAdd, ...experimentCollection];
    }
    return experimentCollection;
  }

  protected convertDateFromClient(experiment: IExperiment): IExperiment {
    return Object.assign({}, experiment, {
      startDate: experiment.startDate?.isValid() ? experiment.startDate.toJSON() : undefined,
      finishCaptureDate: experiment.finishCaptureDate?.isValid() ? experiment.finishCaptureDate.toJSON() : undefined,
      finishDate: experiment.finishDate?.isValid() ? experiment.finishDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? dayjs(res.body.startDate) : undefined;
      res.body.finishCaptureDate = res.body.finishCaptureDate ? dayjs(res.body.finishCaptureDate) : undefined;
      res.body.finishDate = res.body.finishDate ? dayjs(res.body.finishDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((experiment: IExperiment) => {
        experiment.startDate = experiment.startDate ? dayjs(experiment.startDate) : undefined;
        experiment.finishCaptureDate = experiment.finishCaptureDate ? dayjs(experiment.finishCaptureDate) : undefined;
        experiment.finishDate = experiment.finishDate ? dayjs(experiment.finishDate) : undefined;
      });
    }
    return res;
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ExperimentStatus } from 'app/entities/enumerations/experiment-status.model';
import { IExperiment, Experiment } from '../experiment.model';

import { ExperimentService } from './experiment.service';

describe('Service Tests', () => {
  describe('Experiment Service', () => {
    let service: ExperimentService;
    let httpMock: HttpTestingController;
    let elemDefault: IExperiment;
    let expectedResult: IExperiment | IExperiment[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ExperimentService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 'AAAAAAA',
        name: 'AAAAAAA',
        description: 'AAAAAAA',
        startDate: currentDate,
        finishCaptureDate: currentDate,
        finishDate: currentDate,
        status: ExperimentStatus.CREATED,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            startDate: currentDate.format(DATE_TIME_FORMAT),
            finishCaptureDate: currentDate.format(DATE_TIME_FORMAT),
            finishDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Experiment', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            startDate: currentDate.format(DATE_TIME_FORMAT),
            finishCaptureDate: currentDate.format(DATE_TIME_FORMAT),
            finishDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startDate: currentDate,
            finishCaptureDate: currentDate,
            finishDate: currentDate,
          },
          returnedFromService
        );

        service.create(new Experiment()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Experiment', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            description: 'BBBBBB',
            startDate: currentDate.format(DATE_TIME_FORMAT),
            finishCaptureDate: currentDate.format(DATE_TIME_FORMAT),
            finishDate: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startDate: currentDate,
            finishCaptureDate: currentDate,
            finishDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Experiment', () => {
        const patchObject = Object.assign(
          {
            finishCaptureDate: currentDate.format(DATE_TIME_FORMAT),
            finishDate: currentDate.format(DATE_TIME_FORMAT),
          },
          new Experiment()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            startDate: currentDate,
            finishCaptureDate: currentDate,
            finishDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Experiment', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            description: 'BBBBBB',
            startDate: currentDate.format(DATE_TIME_FORMAT),
            finishCaptureDate: currentDate.format(DATE_TIME_FORMAT),
            finishDate: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startDate: currentDate,
            finishCaptureDate: currentDate,
            finishDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Experiment', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addExperimentToCollectionIfMissing', () => {
        it('should add a Experiment to an empty array', () => {
          const experiment: IExperiment = { id: 'ABC' };
          expectedResult = service.addExperimentToCollectionIfMissing([], experiment);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(experiment);
        });

        it('should not add a Experiment to an array that contains it', () => {
          const experiment: IExperiment = { id: 'ABC' };
          const experimentCollection: IExperiment[] = [
            {
              ...experiment,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addExperimentToCollectionIfMissing(experimentCollection, experiment);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Experiment to an array that doesn't contain it", () => {
          const experiment: IExperiment = { id: 'ABC' };
          const experimentCollection: IExperiment[] = [{ id: 'CBA' }];
          expectedResult = service.addExperimentToCollectionIfMissing(experimentCollection, experiment);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(experiment);
        });

        it('should add only unique Experiment to an array', () => {
          const experimentArray: IExperiment[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'Investment Tennessee Home' }];
          const experimentCollection: IExperiment[] = [{ id: 'ABC' }];
          expectedResult = service.addExperimentToCollectionIfMissing(experimentCollection, ...experimentArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const experiment: IExperiment = { id: 'ABC' };
          const experiment2: IExperiment = { id: 'CBA' };
          expectedResult = service.addExperimentToCollectionIfMissing([], experiment, experiment2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(experiment);
          expect(expectedResult).toContain(experiment2);
        });

        it('should accept null and undefined values', () => {
          const experiment: IExperiment = { id: 'ABC' };
          expectedResult = service.addExperimentToCollectionIfMissing([], null, experiment, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(experiment);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

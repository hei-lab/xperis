import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'institution',
        data: { pageTitle: 'xperisApp.institution.home.title' },
        loadChildren: () => import('./institution/institution.module').then(m => m.InstitutionModule),
      },
      {
        path: 'experiment',
        data: { pageTitle: 'xperisApp.experiment.home.title' },
        loadChildren: () => import('./experiment/experiment.module').then(m => m.ExperimentModule),
      },
      {
        path: 'experiment-meta-data',
        data: { pageTitle: 'xperisApp.experimentMetaData.home.title' },
        loadChildren: () => import('./experiment-meta-data/experiment-meta-data.module').then(m => m.ExperimentMetaDataModule),
      },
      {
        path: 'experiment-data',
        data: { pageTitle: 'xperisApp.experimentData.home.title' },
        loadChildren: () => import('./experiment-data/experiment-data.module').then(m => m.ExperimentDataModule),
      },
      {
        path: 'data-mime-type',
        data: { pageTitle: 'xperisApp.dataMimeType.home.title' },
        loadChildren: () => import('./data-mime-type/data-mime-type.module').then(m => m.DataMimeTypeModule),
      },
      {
        path: 'data-template',
        data: { pageTitle: 'xperisApp.dataTemplate.home.title' },
        loadChildren: () => import('./data-template/data-template.module').then(m => m.DataTemplateModule),
      },
      {
        path: 'plugin',
        data: { pageTitle: 'xperisApp.plugin.home.title' },
        loadChildren: () => import('./plugin/plugin.module').then(m => m.PluginModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}

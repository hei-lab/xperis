import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PluginComponent } from './list/plugin.component';
import { PluginDetailComponent } from './detail/plugin-detail.component';
import { PluginUpdateComponent } from './update/plugin-update.component';
import { PluginDeleteDialogComponent } from './delete/plugin-delete-dialog.component';
import { PluginRoutingModule } from './route/plugin-routing.module';

@NgModule({
  imports: [SharedModule, PluginRoutingModule],
  declarations: [PluginComponent, PluginDetailComponent, PluginUpdateComponent, PluginDeleteDialogComponent],
  entryComponents: [PluginDeleteDialogComponent],
})
export class PluginModule {}

jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPlugin, Plugin } from '../plugin.model';
import { PluginService } from '../service/plugin.service';

import { PluginRoutingResolveService } from './plugin-routing-resolve.service';

describe('Service Tests', () => {
  describe('Plugin routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PluginRoutingResolveService;
    let service: PluginService;
    let resultPlugin: IPlugin | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PluginRoutingResolveService);
      service = TestBed.inject(PluginService);
      resultPlugin = undefined;
    });

    describe('resolve', () => {
      it('should return IPlugin returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPlugin = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultPlugin).toEqual({ id: 'ABC' });
      });

      it('should return new IPlugin if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPlugin = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPlugin).toEqual(new Plugin());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPlugin = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultPlugin).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});

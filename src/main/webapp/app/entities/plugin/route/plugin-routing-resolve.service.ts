import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPlugin, Plugin } from '../plugin.model';
import { PluginService } from '../service/plugin.service';

@Injectable({ providedIn: 'root' })
export class PluginRoutingResolveService implements Resolve<IPlugin> {
  constructor(protected service: PluginService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPlugin> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((plugin: HttpResponse<Plugin>) => {
          if (plugin.body) {
            return of(plugin.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Plugin());
  }
}

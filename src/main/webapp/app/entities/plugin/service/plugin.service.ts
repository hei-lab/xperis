import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPlugin, getPluginIdentifier } from '../plugin.model';

export type EntityResponseType = HttpResponse<IPlugin>;
export type EntityArrayResponseType = HttpResponse<IPlugin[]>;

@Injectable({ providedIn: 'root' })
export class PluginService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/plugins');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(plugin: IPlugin): Observable<EntityResponseType> {
    return this.http.post<IPlugin>(this.resourceUrl, plugin, { observe: 'response' });
  }

  update(plugin: IPlugin): Observable<EntityResponseType> {
    return this.http.put<IPlugin>(`${this.resourceUrl}/${getPluginIdentifier(plugin) as string}`, plugin, {
      observe: 'response',
    });
  }

  partialUpdate(plugin: IPlugin): Observable<EntityResponseType> {
    return this.http.patch<IPlugin>(`${this.resourceUrl}/${getPluginIdentifier(plugin) as string}`, plugin, {
      observe: 'response',
      headers: { 'Content-Type': 'application/merge-patch+json' },
    });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IPlugin>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPlugin[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  queryAll(mimetypeid: string = ''): Observable<EntityArrayResponseType> {
    const options = new HttpParams().set('mimetype', mimetypeid);
    return this.http.get<IPlugin[]>(`${this.resourceUrl}/all`, { params: options, observe: 'response' });
  }

  reload(): Observable<any> {
    return this.http.post(`${this.resourceUrl}/load`, { observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPluginToCollectionIfMissing(pluginCollection: IPlugin[], ...pluginsToCheck: (IPlugin | null | undefined)[]): IPlugin[] {
    const plugins: IPlugin[] = pluginsToCheck.filter(isPresent);
    if (plugins.length > 0) {
      const pluginCollectionIdentifiers = pluginCollection.map(pluginItem => getPluginIdentifier(pluginItem)!);
      const pluginsToAdd = plugins.filter(pluginItem => {
        const pluginIdentifier = getPluginIdentifier(pluginItem);
        if (pluginIdentifier == null || pluginCollectionIdentifiers.includes(pluginIdentifier)) {
          return false;
        }
        pluginCollectionIdentifiers.push(pluginIdentifier);
        return true;
      });
      return [...pluginsToAdd, ...pluginCollection];
    }
    return pluginCollection;
  }
}

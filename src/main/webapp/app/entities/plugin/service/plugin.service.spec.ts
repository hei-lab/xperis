import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPlugin, Plugin } from '../plugin.model';

import { PluginService } from './plugin.service';

describe('Service Tests', () => {
  describe('Plugin Service', () => {
    let service: PluginService;
    let httpMock: HttpTestingController;
    let elemDefault: IPlugin;
    let expectedResult: IPlugin | IPlugin[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PluginService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
        name: 'AAAAAAA',
        className: 'AAAAAAA',
        fileName: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Plugin', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Plugin()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Plugin', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            className: 'BBBBBB',
            fileName: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Plugin', () => {
        const patchObject = Object.assign(
          {
            name: 'BBBBBB',
          },
          new Plugin()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Plugin', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            className: 'BBBBBB',
            fileName: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Plugin', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPluginToCollectionIfMissing', () => {
        it('should add a Plugin to an empty array', () => {
          const plugin: IPlugin = { id: 'ABC' };
          expectedResult = service.addPluginToCollectionIfMissing([], plugin);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(plugin);
        });

        it('should not add a Plugin to an array that contains it', () => {
          const plugin: IPlugin = { id: 'ABC' };
          const pluginCollection: IPlugin[] = [
            {
              ...plugin,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addPluginToCollectionIfMissing(pluginCollection, plugin);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Plugin to an array that doesn't contain it", () => {
          const plugin: IPlugin = { id: 'ABC' };
          const pluginCollection: IPlugin[] = [{ id: 'CBA' }];
          expectedResult = service.addPluginToCollectionIfMissing(pluginCollection, plugin);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(plugin);
        });

        it('should add only unique Plugin to an array', () => {
          const pluginArray: IPlugin[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'Loan' }];
          const pluginCollection: IPlugin[] = [{ id: 'ABC' }];
          expectedResult = service.addPluginToCollectionIfMissing(pluginCollection, ...pluginArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const plugin: IPlugin = { id: 'ABC' };
          const plugin2: IPlugin = { id: 'CBA' };
          expectedResult = service.addPluginToCollectionIfMissing([], plugin, plugin2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(plugin);
          expect(expectedResult).toContain(plugin2);
        });

        it('should accept null and undefined values', () => {
          const plugin: IPlugin = { id: 'ABC' };
          expectedResult = service.addPluginToCollectionIfMissing([], null, plugin, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(plugin);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

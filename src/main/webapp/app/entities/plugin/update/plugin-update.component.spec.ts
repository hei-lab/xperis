jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PluginService } from '../service/plugin.service';
import { IPlugin, Plugin } from '../plugin.model';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { DataMimeTypeService } from 'app/entities/data-mime-type/service/data-mime-type.service';

import { PluginUpdateComponent } from './plugin-update.component';

describe('Component Tests', () => {
  describe('Plugin Management Update Component', () => {
    let comp: PluginUpdateComponent;
    let fixture: ComponentFixture<PluginUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let pluginService: PluginService;
    let dataMimeTypeService: DataMimeTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PluginUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PluginUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PluginUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      pluginService = TestBed.inject(PluginService);
      dataMimeTypeService = TestBed.inject(DataMimeTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call DataMimeType query and add missing value', () => {
        const plugin: IPlugin = { id: 'CBA' };
        const dataMimeType: IDataMimeType = { id: 'copy' };
        plugin.dataMimeType = dataMimeType;

        const dataMimeTypeCollection: IDataMimeType[] = [{ id: 'Generic' }];
        spyOn(dataMimeTypeService, 'query').and.returnValue(of(new HttpResponse({ body: dataMimeTypeCollection })));
        const additionalDataMimeTypes = [dataMimeType];
        const expectedCollection: IDataMimeType[] = [...additionalDataMimeTypes, ...dataMimeTypeCollection];
        spyOn(dataMimeTypeService, 'addDataMimeTypeToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ plugin });
        comp.ngOnInit();

        expect(dataMimeTypeService.query).toHaveBeenCalled();
        expect(dataMimeTypeService.addDataMimeTypeToCollectionIfMissing).toHaveBeenCalledWith(
          dataMimeTypeCollection,
          ...additionalDataMimeTypes
        );
        expect(comp.dataMimeTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const plugin: IPlugin = { id: 'CBA' };
        const dataMimeType: IDataMimeType = { id: 'Fully-configurable' };
        plugin.dataMimeType = dataMimeType;

        activatedRoute.data = of({ plugin });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(plugin));
        expect(comp.dataMimeTypesSharedCollection).toContain(dataMimeType);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const plugin = { id: 'ABC' };
        spyOn(pluginService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ plugin });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: plugin }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(pluginService.update).toHaveBeenCalledWith(plugin);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const plugin = new Plugin();
        spyOn(pluginService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ plugin });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: plugin }));
        saveSubject.complete();

        // THEN
        expect(pluginService.create).toHaveBeenCalledWith(plugin);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const plugin = { id: 'ABC' };
        spyOn(pluginService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ plugin });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(pluginService.update).toHaveBeenCalledWith(plugin);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackDataMimeTypeById', () => {
        it('Should return tracked DataMimeType primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackDataMimeTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});

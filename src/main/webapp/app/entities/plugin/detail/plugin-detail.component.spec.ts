import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DataUtils } from 'app/core/util/data-util.service';

import { PluginDetailComponent } from './plugin-detail.component';

describe('Component Tests', () => {
  describe('Plugin Management Detail Component', () => {
    let comp: PluginDetailComponent;
    let fixture: ComponentFixture<PluginDetailComponent>;
    let dataUtils: DataUtils;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PluginDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ plugin: { id: 'ABC' } }) },
          },
        ],
      })
        .overrideTemplate(PluginDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PluginDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = TestBed.inject(DataUtils);
    });

    describe('OnInit', () => {
      it('Should load plugin on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.plugin).toEqual(jasmine.objectContaining({ id: 'ABC' }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from DataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from DataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeBase64, fakeContentType);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeBase64, fakeContentType);
      });
    });
  });
});

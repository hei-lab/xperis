import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPlugin } from '../plugin.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-plugin-detail',
  templateUrl: './plugin-detail.component.html',
})
export class PluginDetailComponent implements OnInit {
  plugin: IPlugin | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ plugin }) => {
      this.plugin = plugin;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(fileName: string | undefined, base64String: string): void {
    this.dataUtils.openFileExt(base64String, null, fileName);
  }

  previousState(): void {
    window.history.back();
  }
}

import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';

export interface IPlugin {
  id?: string;
  name?: string;
  className?: string;
  fileName?: string | null;
  data?: string | null;
  dataMimeType?: IDataMimeType;
  activated?: boolean;
  valid?: boolean;
}

export class Plugin implements IPlugin {
  constructor(
    public id?: string,
    public name?: string,
    public className?: string,
    public fileName?: string | null,
    public data?: string | null,
    public dataMimeType?: IDataMimeType,
    public activated?: boolean,
    public valid?: boolean
  ) {}
}

export function getPluginIdentifier(plugin: IPlugin): string | undefined {
  return plugin.id;
}

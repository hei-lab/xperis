jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { PluginService } from '../service/plugin.service';

import { PluginDeleteDialogComponent } from './plugin-delete-dialog.component';

describe('Component Tests', () => {
  describe('Plugin Management Delete Component', () => {
    let comp: PluginDeleteDialogComponent;
    let fixture: ComponentFixture<PluginDeleteDialogComponent>;
    let service: PluginService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PluginDeleteDialogComponent],
        providers: [NgbActiveModal],
      })
        .overrideTemplate(PluginDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PluginDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(PluginService);
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete('ABC');
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith('ABC');
          expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.close).not.toHaveBeenCalled();
        expect(mockActiveModal.dismiss).toHaveBeenCalled();
      });
    });
  });
});

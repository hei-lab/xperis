import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPlugin } from '../plugin.model';
import { PluginService } from '../service/plugin.service';

@Component({
  templateUrl: './plugin-delete-dialog.component.html',
})
export class PluginDeleteDialogComponent {
  plugin?: IPlugin;

  constructor(protected pluginService: PluginService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.pluginService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

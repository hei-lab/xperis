import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DataMimeTypeDetailComponent } from './data-mime-type-detail.component';

describe('Component Tests', () => {
  describe('DataMimeType Management Detail Component', () => {
    let comp: DataMimeTypeDetailComponent;
    let fixture: ComponentFixture<DataMimeTypeDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DataMimeTypeDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dataMimeType: { id: 'ABC' } }) },
          },
        ],
      })
        .overrideTemplate(DataMimeTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DataMimeTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dataMimeType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dataMimeType).toEqual(jasmine.objectContaining({ id: 'ABC' }));
      });
    });
  });
});

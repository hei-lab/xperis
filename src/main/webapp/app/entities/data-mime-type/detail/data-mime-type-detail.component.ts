import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDataMimeType } from '../data-mime-type.model';

@Component({
  selector: 'jhi-data-mime-type-detail',
  templateUrl: './data-mime-type-detail.component.html',
})
export class DataMimeTypeDetailComponent implements OnInit {
  dataMimeType: IDataMimeType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dataMimeType }) => {
      this.dataMimeType = dataMimeType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDataMimeType } from '../data-mime-type.model';
import { DataMimeTypeService } from '../service/data-mime-type.service';

@Component({
  templateUrl: './data-mime-type-delete-dialog.component.html',
})
export class DataMimeTypeDeleteDialogComponent {
  dataMimeType?: IDataMimeType;

  constructor(protected dataMimeTypeService: DataMimeTypeService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.dataMimeTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { DataMimeTypeService } from '../service/data-mime-type.service';

import { DataMimeTypeDeleteDialogComponent } from './data-mime-type-delete-dialog.component';

describe('Component Tests', () => {
  describe('DataMimeType Management Delete Component', () => {
    let comp: DataMimeTypeDeleteDialogComponent;
    let fixture: ComponentFixture<DataMimeTypeDeleteDialogComponent>;
    let service: DataMimeTypeService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DataMimeTypeDeleteDialogComponent],
        providers: [NgbActiveModal],
      })
        .overrideTemplate(DataMimeTypeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DataMimeTypeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DataMimeTypeService);
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete('ABC');
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith('ABC');
          expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.close).not.toHaveBeenCalled();
        expect(mockActiveModal.dismiss).toHaveBeenCalled();
      });
    });
  });
});

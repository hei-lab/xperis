import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDataMimeType, getDataMimeTypeIdentifier } from '../data-mime-type.model';

export type EntityResponseType = HttpResponse<IDataMimeType>;
export type EntityArrayResponseType = HttpResponse<IDataMimeType[]>;

@Injectable({ providedIn: 'root' })
export class DataMimeTypeService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/data-mime-types');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(dataMimeType: IDataMimeType): Observable<EntityResponseType> {
    return this.http.post<IDataMimeType>(this.resourceUrl, dataMimeType, { observe: 'response' });
  }

  update(dataMimeType: IDataMimeType): Observable<EntityResponseType> {
    return this.http.put<IDataMimeType>(`${this.resourceUrl}/${getDataMimeTypeIdentifier(dataMimeType) as string}`, dataMimeType, {
      observe: 'response',
    });
  }

  partialUpdate(dataMimeType: IDataMimeType): Observable<EntityResponseType> {
    return this.http.patch<IDataMimeType>(`${this.resourceUrl}/${getDataMimeTypeIdentifier(dataMimeType) as string}`, dataMimeType, {
      observe: 'response',
      headers: { 'Content-Type': 'application/merge-patch+json' },
    });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IDataMimeType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDataMimeType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  queryAll(): Observable<EntityArrayResponseType> {
    return this.http.get<IDataMimeType[]>(`${this.resourceUrl}/all`, { observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDataMimeTypeToCollectionIfMissing(
    dataMimeTypeCollection: IDataMimeType[],
    ...dataMimeTypesToCheck: (IDataMimeType | null | undefined)[]
  ): IDataMimeType[] {
    const dataMimeTypes: IDataMimeType[] = dataMimeTypesToCheck.filter(isPresent);
    if (dataMimeTypes.length > 0) {
      const dataMimeTypeCollectionIdentifiers = dataMimeTypeCollection.map(
        dataMimeTypeItem => getDataMimeTypeIdentifier(dataMimeTypeItem)!
      );
      const dataMimeTypesToAdd = dataMimeTypes.filter(dataMimeTypeItem => {
        const dataMimeTypeIdentifier = getDataMimeTypeIdentifier(dataMimeTypeItem);
        if (dataMimeTypeIdentifier == null || dataMimeTypeCollectionIdentifiers.includes(dataMimeTypeIdentifier)) {
          return false;
        }
        dataMimeTypeCollectionIdentifiers.push(dataMimeTypeIdentifier);
        return true;
      });
      return [...dataMimeTypesToAdd, ...dataMimeTypeCollection];
    }
    return dataMimeTypeCollection;
  }
}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDataMimeType, DataMimeType } from '../data-mime-type.model';
import { DataMimeTypeService } from '../service/data-mime-type.service';

@Injectable({ providedIn: 'root' })
export class DataMimeTypeRoutingResolveService implements Resolve<IDataMimeType> {
  constructor(protected service: DataMimeTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDataMimeType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dataMimeType: HttpResponse<DataMimeType>) => {
          if (dataMimeType.body) {
            return of(dataMimeType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DataMimeType());
  }
}

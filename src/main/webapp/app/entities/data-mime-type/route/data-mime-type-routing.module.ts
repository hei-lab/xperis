import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DataMimeTypeComponent } from '../list/data-mime-type.component';
import { DataMimeTypeDetailComponent } from '../detail/data-mime-type-detail.component';
import { DataMimeTypeUpdateComponent } from '../update/data-mime-type-update.component';
import { DataMimeTypeRoutingResolveService } from './data-mime-type-routing-resolve.service';

const dataMimeTypeRoute: Routes = [
  {
    path: '',
    component: DataMimeTypeComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DataMimeTypeDetailComponent,
    resolve: {
      dataMimeType: DataMimeTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DataMimeTypeUpdateComponent,
    resolve: {
      dataMimeType: DataMimeTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DataMimeTypeUpdateComponent,
    resolve: {
      dataMimeType: DataMimeTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dataMimeTypeRoute)],
  exports: [RouterModule],
})
export class DataMimeTypeRoutingModule {}

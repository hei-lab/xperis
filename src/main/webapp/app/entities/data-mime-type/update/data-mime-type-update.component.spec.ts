jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DataMimeTypeService } from '../service/data-mime-type.service';
import { IDataMimeType, DataMimeType } from '../data-mime-type.model';

import { DataMimeTypeUpdateComponent } from './data-mime-type-update.component';

describe('Component Tests', () => {
  describe('DataMimeType Management Update Component', () => {
    let comp: DataMimeTypeUpdateComponent;
    let fixture: ComponentFixture<DataMimeTypeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dataMimeTypeService: DataMimeTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DataMimeTypeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DataMimeTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DataMimeTypeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dataMimeTypeService = TestBed.inject(DataMimeTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const dataMimeType: IDataMimeType = { id: 'CBA' };

        activatedRoute.data = of({ dataMimeType });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dataMimeType));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dataMimeType = { id: 'ABC' };
        spyOn(dataMimeTypeService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dataMimeType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dataMimeType }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dataMimeTypeService.update).toHaveBeenCalledWith(dataMimeType);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dataMimeType = new DataMimeType();
        spyOn(dataMimeTypeService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dataMimeType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dataMimeType }));
        saveSubject.complete();

        // THEN
        expect(dataMimeTypeService.create).toHaveBeenCalledWith(dataMimeType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dataMimeType = { id: 'ABC' };
        spyOn(dataMimeTypeService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dataMimeType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dataMimeTypeService.update).toHaveBeenCalledWith(dataMimeType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});

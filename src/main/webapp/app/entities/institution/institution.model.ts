import { IUser } from 'app/entities/user/user.model';

export interface IInstitution {
  id?: number;
  name?: string;
  employees?: IUser[] | null;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Institution implements IInstitution {
  constructor(
    public id?: number,
    public name?: string,
    public employees?: IUser[] | null,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {}
}

export function getInstitutionIdentifier(institution: IInstitution): number | undefined {
  return institution.id;
}

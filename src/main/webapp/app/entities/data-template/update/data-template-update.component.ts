import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { DataTemplate, IDataTemplate } from '../data-template.model';
import { DataTemplateService } from '../service/data-template.service';
import { EventManager } from 'app/core/util/event-manager.service';
import { DataUtils } from 'app/core/util/data-util.service';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { DataMimeTypeService } from 'app/entities/data-mime-type/service/data-mime-type.service';

@Component({
  selector: 'jhi-data-template-update',
  templateUrl: './data-template-update.component.html',
})
export class DataTemplateUpdateComponent implements OnInit {
  isSaving = false;

  dataMimeTypesSharedCollection: IDataMimeType[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    dataStructure: [null, [Validators.required]],
    viewerClasses: [],
    dataMimeType: [null, Validators.required],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected dataTemplateService: DataTemplateService,
    protected dataMimeTypeService: DataMimeTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dataTemplate }) => {
      this.updateForm(dataTemplate);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dataTemplate = this.createFromForm();
    if (dataTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.dataTemplateService.partialUpdate(dataTemplate));
    } else {
      this.subscribeToSaveResponse(this.dataTemplateService.create(dataTemplate));
    }
  }

  trackDataMimeTypeById(index: number, item: IDataMimeType): string {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDataTemplate>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dataTemplate: IDataTemplate): void {
    this.editForm.patchValue({
      id: dataTemplate.id,
      name: dataTemplate.name,
      dataStructure: dataTemplate.dataStructure,
      viewerClasses: dataTemplate.viewerClasses,
      dataMimeType: dataTemplate.dataMimeType,
    });

    this.dataMimeTypesSharedCollection = this.dataMimeTypeService.addDataMimeTypeToCollectionIfMissing(
      this.dataMimeTypesSharedCollection,
      dataTemplate.dataMimeType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.dataMimeTypeService
      .queryAll()
      .pipe(map((res: HttpResponse<IDataMimeType[]>) => res.body ?? []))
      .pipe(
        map((dataMimeTypes: IDataMimeType[]) =>
          this.dataMimeTypeService.addDataMimeTypeToCollectionIfMissing(dataMimeTypes, this.editForm.get('dataMimeType')!.value)
        )
      )
      .subscribe((dataMimeTypes: IDataMimeType[]) => (this.dataMimeTypesSharedCollection = dataMimeTypes));
  }

  protected createFromForm(): IDataTemplate {
    return {
      ...new DataTemplate(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      dataStructure: this.editForm.get(['dataStructure'])!.value,
      viewerClasses: this.editForm.get(['viewerClasses'])!.value,
      dataMimeType: this.editForm.get(['dataMimeType'])!.value,
    };
  }
}

jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DataTemplateService } from '../service/data-template.service';
import { IDataTemplate, DataTemplate } from '../data-template.model';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { DataMimeTypeService } from 'app/entities/data-mime-type/service/data-mime-type.service';

import { DataTemplateUpdateComponent } from './data-template-update.component';

describe('Component Tests', () => {
  describe('DataTemplate Management Update Component', () => {
    let comp: DataTemplateUpdateComponent;
    let fixture: ComponentFixture<DataTemplateUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dataTemplateService: DataTemplateService;
    let dataMimeTypeService: DataMimeTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DataTemplateUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DataTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DataTemplateUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dataTemplateService = TestBed.inject(DataTemplateService);
      dataMimeTypeService = TestBed.inject(DataMimeTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call DataMimeType query and add missing value', () => {
        const dataTemplate: IDataTemplate = { id: 'CBA' };
        const dataMimeType: IDataMimeType = { id: 'copy' };
        dataTemplate.dataMimeType = dataMimeType;

        const dataMimeTypeCollection: IDataMimeType[] = [{ id: 'Generic' }];
        spyOn(dataMimeTypeService, 'query').and.returnValue(of(new HttpResponse({ body: dataMimeTypeCollection })));
        const additionalDataMimeTypes = [dataMimeType];
        const expectedCollection: IDataMimeType[] = [...additionalDataMimeTypes, ...dataMimeTypeCollection];
        spyOn(dataMimeTypeService, 'addDataMimeTypeToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ dataTemplate });
        comp.ngOnInit();

        expect(dataMimeTypeService.query).toHaveBeenCalled();
        expect(dataMimeTypeService.addDataMimeTypeToCollectionIfMissing).toHaveBeenCalledWith(
          dataMimeTypeCollection,
          ...additionalDataMimeTypes
        );
        expect(comp.dataMimeTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const dataTemplate: IDataTemplate = { id: 'CBA' };
        const dataMimeType: IDataMimeType = { id: 'Fully-configurable' };
        dataTemplate.dataMimeType = dataMimeType;

        activatedRoute.data = of({ dataTemplate });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dataTemplate));
        expect(comp.dataMimeTypesSharedCollection).toContain(dataMimeType);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dataTemplate = { id: 'ABC' };
        spyOn(dataTemplateService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dataTemplate });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dataTemplate }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dataTemplateService.update).toHaveBeenCalledWith(dataTemplate);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dataTemplate = new DataTemplate();
        spyOn(dataTemplateService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dataTemplate });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dataTemplate }));
        saveSubject.complete();

        // THEN
        expect(dataTemplateService.create).toHaveBeenCalledWith(dataTemplate);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dataTemplate = { id: 'ABC' };
        spyOn(dataTemplateService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dataTemplate });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dataTemplateService.update).toHaveBeenCalledWith(dataTemplate);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackDataMimeTypeById', () => {
        it('Should return tracked DataMimeType primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackDataMimeTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDataTemplate } from '../data-template.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-data-template-detail',
  templateUrl: './data-template-detail.component.html',
})
export class DataTemplateDetailComponent implements OnInit {
  dataTemplate: IDataTemplate | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dataTemplate }) => {
      this.dataTemplate = dataTemplate;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDataTemplate, DataTemplate } from '../data-template.model';
import { DataTemplateService } from '../service/data-template.service';

@Injectable({ providedIn: 'root' })
export class DataTemplateRoutingResolveService implements Resolve<IDataTemplate> {
  constructor(protected service: DataTemplateService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDataTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dataTemplate: HttpResponse<DataTemplate>) => {
          if (dataTemplate.body) {
            return of(dataTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DataTemplate());
  }
}

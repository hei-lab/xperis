import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DataTemplateComponent } from '../list/data-template.component';
import { DataTemplateDetailComponent } from '../detail/data-template-detail.component';
import { DataTemplateUpdateComponent } from '../update/data-template-update.component';
import { DataTemplateRoutingResolveService } from './data-template-routing-resolve.service';

const dataTemplateRoute: Routes = [
  {
    path: '',
    component: DataTemplateComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DataTemplateDetailComponent,
    resolve: {
      dataTemplate: DataTemplateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DataTemplateUpdateComponent,
    resolve: {
      dataTemplate: DataTemplateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DataTemplateUpdateComponent,
    resolve: {
      dataTemplate: DataTemplateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dataTemplateRoute)],
  exports: [RouterModule],
})
export class DataTemplateRoutingModule {}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDataTemplate, getDataTemplateIdentifier } from '../data-template.model';

export type EntityResponseType = HttpResponse<IDataTemplate>;
export type EntityArrayResponseType = HttpResponse<IDataTemplate[]>;

@Injectable({ providedIn: 'root' })
export class DataTemplateService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/data-templates');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(dataTemplate: IDataTemplate): Observable<EntityResponseType> {
    return this.http.post<IDataTemplate>(this.resourceUrl, dataTemplate, { observe: 'response' });
  }

  update(dataTemplate: IDataTemplate): Observable<EntityResponseType> {
    return this.http.put<IDataTemplate>(`${this.resourceUrl}/${getDataTemplateIdentifier(dataTemplate) as string}`, dataTemplate, {
      observe: 'response',
    });
  }

  partialUpdate(dataTemplate: IDataTemplate): Observable<EntityResponseType> {
    return this.http.patch<IDataTemplate>(`${this.resourceUrl}/${getDataTemplateIdentifier(dataTemplate) as string}`, dataTemplate, {
      observe: 'response',
      headers: { 'Content-Type': 'application/merge-patch+json' },
    });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IDataTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDataTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDataTemplateToCollectionIfMissing(
    dataTemplateCollection: IDataTemplate[],
    ...dataTemplatesToCheck: (IDataTemplate | null | undefined)[]
  ): IDataTemplate[] {
    const dataTemplates: IDataTemplate[] = dataTemplatesToCheck.filter(isPresent);
    if (dataTemplates.length > 0) {
      const dataTemplateCollectionIdentifiers = dataTemplateCollection.map(
        dataTemplateItem => getDataTemplateIdentifier(dataTemplateItem)!
      );
      const dataTemplatesToAdd = dataTemplates.filter(dataTemplateItem => {
        const dataTemplateIdentifier = getDataTemplateIdentifier(dataTemplateItem);
        if (dataTemplateIdentifier == null || dataTemplateCollectionIdentifiers.includes(dataTemplateIdentifier)) {
          return false;
        }
        dataTemplateCollectionIdentifiers.push(dataTemplateIdentifier);
        return true;
      });
      return [...dataTemplatesToAdd, ...dataTemplateCollection];
    }
    return dataTemplateCollection;
  }
}

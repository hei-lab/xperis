import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDataTemplate, DataTemplate } from '../data-template.model';

import { DataTemplateService } from './data-template.service';

describe('Service Tests', () => {
  describe('DataTemplate Service', () => {
    let service: DataTemplateService;
    let httpMock: HttpTestingController;
    let elemDefault: IDataTemplate;
    let expectedResult: IDataTemplate | IDataTemplate[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DataTemplateService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
        name: 'AAAAAAA',
        dataStructure: 'AAAAAAA',
        viewerClasses: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DataTemplate', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new DataTemplate()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DataTemplate', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            dataStructure: 'BBBBBB',
            viewerClasses: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DataTemplate', () => {
        const patchObject = Object.assign(
          {
            name: 'BBBBBB',
          },
          new DataTemplate()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DataTemplate', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            dataStructure: 'BBBBBB',
            viewerClasses: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DataTemplate', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDataTemplateToCollectionIfMissing', () => {
        it('should add a DataTemplate to an empty array', () => {
          const dataTemplate: IDataTemplate = { id: 'ABC' };
          expectedResult = service.addDataTemplateToCollectionIfMissing([], dataTemplate);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dataTemplate);
        });

        it('should not add a DataTemplate to an array that contains it', () => {
          const dataTemplate: IDataTemplate = { id: 'ABC' };
          const dataTemplateCollection: IDataTemplate[] = [
            {
              ...dataTemplate,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addDataTemplateToCollectionIfMissing(dataTemplateCollection, dataTemplate);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DataTemplate to an array that doesn't contain it", () => {
          const dataTemplate: IDataTemplate = { id: 'ABC' };
          const dataTemplateCollection: IDataTemplate[] = [{ id: 'CBA' }];
          expectedResult = service.addDataTemplateToCollectionIfMissing(dataTemplateCollection, dataTemplate);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dataTemplate);
        });

        it('should add only unique DataTemplate to an array', () => {
          const dataTemplateArray: IDataTemplate[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'Loan' }];
          const dataTemplateCollection: IDataTemplate[] = [{ id: 'ABC' }];
          expectedResult = service.addDataTemplateToCollectionIfMissing(dataTemplateCollection, ...dataTemplateArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dataTemplate: IDataTemplate = { id: 'ABC' };
          const dataTemplate2: IDataTemplate = { id: 'CBA' };
          expectedResult = service.addDataTemplateToCollectionIfMissing([], dataTemplate, dataTemplate2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dataTemplate);
          expect(expectedResult).toContain(dataTemplate2);
        });

        it('should accept null and undefined values', () => {
          const dataTemplate: IDataTemplate = { id: 'ABC' };
          expectedResult = service.addDataTemplateToCollectionIfMissing([], null, dataTemplate, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dataTemplate);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

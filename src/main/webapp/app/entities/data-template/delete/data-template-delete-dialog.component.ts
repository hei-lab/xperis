import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDataTemplate } from '../data-template.model';
import { DataTemplateService } from '../service/data-template.service';

@Component({
  templateUrl: './data-template-delete-dialog.component.html',
})
export class DataTemplateDeleteDialogComponent {
  dataTemplate?: IDataTemplate;

  constructor(protected dataTemplateService: DataTemplateService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.dataTemplateService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

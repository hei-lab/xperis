export enum RegisterType {
  NO_REGISTER = 'NO_REGISTER',

  TOKEN = 'TOKEN',

  SELF_REGISTER = 'SELF_REGISTER',
}

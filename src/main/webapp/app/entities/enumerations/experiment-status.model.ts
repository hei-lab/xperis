export enum ExperimentStatus {
  CREATED = 'CREATED',

  CONFIRMED = 'CONFIRMED',

  STARTED = 'STARTED',

  FINISHED = 'FINISHED',
}

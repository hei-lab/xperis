import * as dayjs from 'dayjs';
import { LogAction } from 'app/entities/enumerations/log-action.model';

export interface IExperimentDataLog {
  id?: string;
  logAction?: LogAction;
  experimentDataId?: string;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string;
  lastModifiedDate?: dayjs.Dayjs | null;
}

export class ExperimentDataLog implements IExperimentDataLog {
  constructor(
    public id?: string,
    public logAction?: LogAction,
    public experimentDataId?: string,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string,
    public lastModifiedDate?: dayjs.Dayjs | null
  ) {}
}

export function getExperimentDataLogIdentifier(ExperimentDataLog: IExperimentDataLog): string | undefined {
  return ExperimentDataLog.id;
}

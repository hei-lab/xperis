import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExperimentData } from '../experiment-data.model';
import { DataUtils } from 'app/core/util/data-util.service';
import { LogAction } from 'app/entities/enumerations/log-action.model';
import { ExperimentDataService } from 'app/entities/experiment-data/service/experiment-data.service';

@Component({
  selector: 'jhi-experiment-data-detail',
  templateUrl: './experiment-data-detail.component.html',
})
export class ExperimentDataDetailComponent implements OnInit {
  experimentData: IExperimentData | null = null;

  constructor(
    protected dataUtils: DataUtils,
    protected activatedRoute: ActivatedRoute,
    protected experimentDataService: ExperimentDataService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ experimentData }) => {
      this.experimentData = experimentData;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(id: string | undefined, base64String: string, contentType: string | null | undefined): void {
    this.experimentDataService.insertLog({ experimentDataId: id, logAction: LogAction.DOWNLOAD_DATA }).subscribe();
    this.dataUtils.openFileExt(base64String, contentType, id);
  }

  previousState(): void {
    window.history.back();
  }
}

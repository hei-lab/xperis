import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IExperimentData, ExperimentData } from '../experiment-data.model';

import { ExperimentDataService } from './experiment-data.service';

describe('Service Tests', () => {
  describe('ExperimentData Service', () => {
    let service: ExperimentDataService;
    let httpMock: HttpTestingController;
    let elemDefault: IExperimentData;
    let expectedResult: IExperimentData | IExperimentData[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ExperimentDataService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
        participants: 'AAAAAAA',
        dataContentType: 'image/png',
        data: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ExperimentData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ExperimentData()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ExperimentData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            participants: 'BBBBBB',
            data: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ExperimentData', () => {
        const patchObject = Object.assign(
          {
            participants: 'BBBBBB',
          },
          new ExperimentData()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ExperimentData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            participants: 'BBBBBB',
            data: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ExperimentData', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addExperimentDataToCollectionIfMissing', () => {
        it('should add a ExperimentData to an empty array', () => {
          const experimentData: IExperimentData = { id: 'ABC' };
          expectedResult = service.addExperimentDataToCollectionIfMissing([], experimentData);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(experimentData);
        });

        it('should not add a ExperimentData to an array that contains it', () => {
          const experimentData: IExperimentData = { id: 'ABC' };
          const experimentDataCollection: IExperimentData[] = [
            {
              ...experimentData,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addExperimentDataToCollectionIfMissing(experimentDataCollection, experimentData);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ExperimentData to an array that doesn't contain it", () => {
          const experimentData: IExperimentData = { id: 'ABC' };
          const experimentDataCollection: IExperimentData[] = [{ id: 'CBA' }];
          expectedResult = service.addExperimentDataToCollectionIfMissing(experimentDataCollection, experimentData);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(experimentData);
        });

        it('should add only unique ExperimentData to an array', () => {
          const experimentDataArray: IExperimentData[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'Estate Home navigating' }];
          const experimentDataCollection: IExperimentData[] = [{ id: 'ABC' }];
          expectedResult = service.addExperimentDataToCollectionIfMissing(experimentDataCollection, ...experimentDataArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const experimentData: IExperimentData = { id: 'ABC' };
          const experimentData2: IExperimentData = { id: 'CBA' };
          expectedResult = service.addExperimentDataToCollectionIfMissing([], experimentData, experimentData2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(experimentData);
          expect(expectedResult).toContain(experimentData2);
        });

        it('should accept null and undefined values', () => {
          const experimentData: IExperimentData = { id: 'ABC' };
          expectedResult = service.addExperimentDataToCollectionIfMissing([], null, experimentData, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(experimentData);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

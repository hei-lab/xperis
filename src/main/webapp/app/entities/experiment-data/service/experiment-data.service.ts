import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IExperimentData, getExperimentDataIdentifier } from '../experiment-data.model';
import { IExperimentDataLog } from 'app/entities/experiment-data/experiment-data-log.model';

export type EntityResponseType = HttpResponse<IExperimentData>;
export type EntityResponseTypeLog = HttpResponse<IExperimentDataLog>;
export type EntityArrayResponseType = HttpResponse<IExperimentData[]>;

@Injectable({ providedIn: 'root' })
export class ExperimentDataService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/experiment-data');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(experimentData: IExperimentData): Observable<EntityResponseType> {
    return this.http.post<IExperimentData>(this.resourceUrl, experimentData, { observe: 'response' });
  }

  update(experimentData: IExperimentData): Observable<EntityResponseType> {
    return this.http.put<IExperimentData>(`${this.resourceUrl}/${getExperimentDataIdentifier(experimentData) as string}`, experimentData, {
      observe: 'response',
    });
  }

  partialUpdate(experimentData: IExperimentData): Observable<EntityResponseType> {
    return this.http.patch<IExperimentData>(
      `${this.resourceUrl}/${getExperimentDataIdentifier(experimentData) as string}`,
      experimentData,
      { observe: 'response', headers: { 'Content-Type': 'application/merge-patch+json' } }
    );
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IExperimentData>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExperimentData[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  insertLog(experimentDataLog: IExperimentDataLog): Observable<EntityResponseTypeLog> {
    return this.http.post<IExperimentDataLog>(`${this.resourceUrl}-log`, experimentDataLog, { observe: 'response' });
  }

  addExperimentDataToCollectionIfMissing(
    experimentDataCollection: IExperimentData[],
    ...experimentDataToCheck: (IExperimentData | null | undefined)[]
  ): IExperimentData[] {
    const experimentData: IExperimentData[] = experimentDataToCheck.filter(isPresent);
    if (experimentData.length > 0) {
      const experimentDataCollectionIdentifiers = experimentDataCollection.map(
        experimentDataItem => getExperimentDataIdentifier(experimentDataItem)!
      );
      const experimentDataToAdd = experimentData.filter(experimentDataItem => {
        const experimentDataIdentifier = getExperimentDataIdentifier(experimentDataItem);
        if (experimentDataIdentifier == null || experimentDataCollectionIdentifiers.includes(experimentDataIdentifier)) {
          return false;
        }
        experimentDataCollectionIdentifiers.push(experimentDataIdentifier);
        return true;
      });
      return [...experimentDataToAdd, ...experimentDataCollection];
    }
    return experimentDataCollection;
  }
}

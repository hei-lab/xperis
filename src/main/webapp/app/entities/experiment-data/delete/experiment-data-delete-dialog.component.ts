import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IExperimentData } from '../experiment-data.model';
import { ExperimentDataService } from '../service/experiment-data.service';

@Component({
  templateUrl: './experiment-data-delete-dialog.component.html',
})
export class ExperimentDataDeleteDialogComponent {
  experimentData?: IExperimentData;

  constructor(protected experimentDataService: ExperimentDataService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.experimentDataService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

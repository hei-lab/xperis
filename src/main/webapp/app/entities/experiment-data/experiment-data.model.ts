import { IExperimentMetaData } from 'app/entities/experiment-meta-data/experiment-meta-data.model';
import { IExperimentDataLog } from 'app/entities/experiment-data/experiment-data-log.model';

export interface IExperimentData {
  id?: string;
  participants?: string | null;
  dataContentType?: string | null;
  data?: string | null;
  logs?: IExperimentDataLog[] | null;
  experimentMetaData?: IExperimentMetaData;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class ExperimentData implements IExperimentData {
  constructor(
    public id?: string,
    public participants?: string | null,
    public dataContentType?: string | null,
    public data?: string | null,
    public logs?: IExperimentDataLog[] | null,
    public experimentMetaData?: IExperimentMetaData,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {}
}

export function getExperimentDataIdentifier(experimentData: IExperimentData): string | undefined {
  return experimentData.id;
}

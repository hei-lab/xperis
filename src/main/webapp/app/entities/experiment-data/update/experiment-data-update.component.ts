import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IExperimentData, ExperimentData } from '../experiment-data.model';
import { ExperimentDataService } from '../service/experiment-data.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IExperimentMetaData } from 'app/entities/experiment-meta-data/experiment-meta-data.model';
import { ExperimentMetaDataService } from 'app/entities/experiment-meta-data/service/experiment-meta-data.service';
import { LogAction } from 'app/entities/enumerations/log-action.model';

@Component({
  selector: 'jhi-experiment-data-update',
  templateUrl: './experiment-data-update.component.html',
})
export class ExperimentDataUpdateComponent implements OnInit {
  isSaving = false;

  experimentMetaDataSharedCollection: IExperimentMetaData[] = [];
  acceptedType = '';

  editForm = this.fb.group({
    id: [],
    participants: [],
    data: [],
    dataContentType: [],
    experimentMetaData: [null, Validators.required],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected experimentDataService: ExperimentDataService,
    protected experimentMetaDataService: ExperimentMetaDataService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ experimentData }) => {
      this.updateMimeType(experimentData.experimentMetaData);
      this.updateForm(experimentData);
      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(id: string | undefined, base64String: string, contentType: string | null | undefined): void {
    this.experimentDataService.insertLog({ experimentDataId: id, logAction: LogAction.DOWNLOAD_DATA }).subscribe();
    this.dataUtils.openFileExt(base64String, contentType, id);
  }

  setFileData(event: Event, field: string): void {
    this.dataUtils
      .loadFileToForm(event, this.editForm, field, this.editForm.get('experimentMetaData')!.value.dataMimeType?.type)
      .subscribe({
        error: (err: FileLoadError) =>
          this.eventManager.broadcast(
            new EventWithContent<AlertError>('xperisApp.error', { ...err, message: err.message })
          ),
      });
  }

  updateMimeType(experimentMetaData: any): void {
    if (experimentMetaData) {
      this.acceptedType = experimentMetaData.dataMimeType?.type.concat(', ').concat(experimentMetaData.dataMimeType?.fileExtension);
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const experimentData = this.createFromForm();
    if (experimentData.id !== undefined) {
      this.subscribeToSaveResponse(this.experimentDataService.partialUpdate(experimentData));
    } else {
      this.subscribeToSaveResponse(this.experimentDataService.create(experimentData));
    }
  }

  trackExperimentMetaDataById(index: number, item: IExperimentMetaData): string {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExperimentData>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(experimentData: IExperimentData): void {
    this.editForm.patchValue({
      id: experimentData.id,
      participants: experimentData.participants,
      data: experimentData.data,
      experimentMetaData: experimentData.experimentMetaData,
      dataContentType: experimentData.experimentMetaData?.dataMimeType?.type,
    });

    this.experimentMetaDataSharedCollection = this.experimentMetaDataService.addExperimentMetaDataToCollectionIfMissing(
      this.experimentMetaDataSharedCollection,
      experimentData.experimentMetaData
    );
  }

  protected loadRelationshipsOptions(): void {
    this.experimentMetaDataService
      .query()
      .pipe(map((res: HttpResponse<IExperimentMetaData[]>) => res.body ?? []))
      .pipe(
        map((experimentMetaData: IExperimentMetaData[]) =>
          this.experimentMetaDataService.addExperimentMetaDataToCollectionIfMissing(
            experimentMetaData,
            this.editForm.get('experimentMetaData')!.value
          )
        )
      )
      .subscribe((experimentMetaData: IExperimentMetaData[]) => (this.experimentMetaDataSharedCollection = experimentMetaData));
  }

  protected createFromForm(): IExperimentData {
    return {
      ...new ExperimentData(),
      id: this.editForm.get(['id'])!.value,
      participants: this.editForm.get(['participants'])!.value,
      dataContentType: this.editForm.get(['dataContentType'])!.value,
      data: this.editForm.get(['data'])!.value,
      experimentMetaData: this.editForm.get(['experimentMetaData'])!.value,
    };
  }
}

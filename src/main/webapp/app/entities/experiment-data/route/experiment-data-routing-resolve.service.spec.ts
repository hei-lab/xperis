jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IExperimentData, ExperimentData } from '../experiment-data.model';
import { ExperimentDataService } from '../service/experiment-data.service';

import { ExperimentDataRoutingResolveService } from './experiment-data-routing-resolve.service';

describe('Service Tests', () => {
  describe('ExperimentData routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ExperimentDataRoutingResolveService;
    let service: ExperimentDataService;
    let resultExperimentData: IExperimentData | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ExperimentDataRoutingResolveService);
      service = TestBed.inject(ExperimentDataService);
      resultExperimentData = undefined;
    });

    describe('resolve', () => {
      it('should return IExperimentData returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultExperimentData = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultExperimentData).toEqual({ id: 'ABC' });
      });

      it('should return new IExperimentData if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultExperimentData = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultExperimentData).toEqual(new ExperimentData());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultExperimentData = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultExperimentData).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});

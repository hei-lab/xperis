import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ExperimentDataComponent } from '../list/experiment-data.component';
import { ExperimentDataDetailComponent } from '../detail/experiment-data-detail.component';
import { ExperimentDataUpdateComponent } from '../update/experiment-data-update.component';
import { ExperimentDataRoutingResolveService } from './experiment-data-routing-resolve.service';

const experimentDataRoute: Routes = [
  {
    path: '',
    component: ExperimentDataComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ExperimentDataDetailComponent,
    resolve: {
      experimentData: ExperimentDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ExperimentDataUpdateComponent,
    resolve: {
      experimentData: ExperimentDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ExperimentDataUpdateComponent,
    resolve: {
      experimentData: ExperimentDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(experimentDataRoute)],
  exports: [RouterModule],
})
export class ExperimentDataRoutingModule {}

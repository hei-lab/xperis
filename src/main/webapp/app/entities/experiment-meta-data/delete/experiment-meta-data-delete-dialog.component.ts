import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IExperimentMetaData } from '../experiment-meta-data.model';
import { ExperimentMetaDataService } from '../service/experiment-meta-data.service';

@Component({
  templateUrl: './experiment-meta-data-delete-dialog.component.html',
})
export class ExperimentMetaDataDeleteDialogComponent {
  experimentMetaData?: IExperimentMetaData;

  constructor(protected experimentMetaDataService: ExperimentMetaDataService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.experimentMetaDataService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

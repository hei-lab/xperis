jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ExperimentMetaDataService } from '../service/experiment-meta-data.service';

import { ExperimentMetaDataDeleteDialogComponent } from './experiment-meta-data-delete-dialog.component';

describe('Component Tests', () => {
  describe('ExperimentMetaData Management Delete Component', () => {
    let comp: ExperimentMetaDataDeleteDialogComponent;
    let fixture: ComponentFixture<ExperimentMetaDataDeleteDialogComponent>;
    let service: ExperimentMetaDataService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ExperimentMetaDataDeleteDialogComponent],
        providers: [NgbActiveModal],
      })
        .overrideTemplate(ExperimentMetaDataDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExperimentMetaDataDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(ExperimentMetaDataService);
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete('ABC');
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith('ABC');
          expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.close).not.toHaveBeenCalled();
        expect(mockActiveModal.dismiss).toHaveBeenCalled();
      });
    });
  });
});

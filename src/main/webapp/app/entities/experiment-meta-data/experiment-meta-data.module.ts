import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ExperimentMetaDataComponent } from './list/experiment-meta-data.component';
import { ExperimentMetaDataDetailComponent } from './detail/experiment-meta-data-detail.component';
import { ExperimentMetaDataUpdateComponent } from './update/experiment-meta-data-update.component';
import { ExperimentMetaDataDeleteDialogComponent } from './delete/experiment-meta-data-delete-dialog.component';
import { ExperimentMetaDataRoutingModule } from './route/experiment-meta-data-routing.module';

@NgModule({
  imports: [SharedModule, ExperimentMetaDataRoutingModule],
  declarations: [
    ExperimentMetaDataComponent,
    ExperimentMetaDataDetailComponent,
    ExperimentMetaDataUpdateComponent,
    ExperimentMetaDataDeleteDialogComponent,
  ],
  entryComponents: [ExperimentMetaDataDeleteDialogComponent],
})
export class ExperimentMetaDataModule {}

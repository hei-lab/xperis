import { IExperimentData } from 'app/entities/experiment-data/experiment-data.model';
import { IExperiment } from 'app/entities/experiment/experiment.model';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { IExperimentDataLog } from 'app/entities/experiment-data/experiment-data-log.model';

export interface IExperimentMetaData {
  id?: string;
  description?: string;
  dataStructure?: string;
  viewerClasses?: string | null;
  experimentData?: IExperimentData[] | null;
  experiment?: IExperiment;
  dataMimeType?: IDataMimeType;
  createdBy?: string;
  logs?: IExperimentDataLog[] | null;
}

export class ExperimentMetaData implements IExperimentMetaData {
  constructor(
    public id?: string,
    public description?: string,
    public dataStructure?: string,
    public viewerClasses?: string | null,
    public experimentData?: IExperimentData[] | null,
    public experiment?: IExperiment,
    public dataMimeType?: IDataMimeType,
    public createdBy?: string,
    public logs?: IExperimentDataLog[] | null
  ) {}
}

export function getExperimentMetaDataIdentifier(experimentMetaData: IExperimentMetaData): string | undefined {
  return experimentMetaData.id;
}

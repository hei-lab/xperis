import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IExperimentMetaData, ExperimentMetaData } from '../experiment-meta-data.model';

import { ExperimentMetaDataService } from './experiment-meta-data.service';

describe('Service Tests', () => {
  describe('ExperimentMetaData Service', () => {
    let service: ExperimentMetaDataService;
    let httpMock: HttpTestingController;
    let elemDefault: IExperimentMetaData;
    let expectedResult: IExperimentMetaData | IExperimentMetaData[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ExperimentMetaDataService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
        description: 'AAAAAAA',
        dataStructure: 'AAAAAAA',
        viewerClasses: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ExperimentMetaData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ExperimentMetaData()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ExperimentMetaData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            description: 'BBBBBB',
            dataStructure: 'BBBBBB',
            viewerClasses: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ExperimentMetaData', () => {
        const patchObject = Object.assign(
          {
            description: 'BBBBBB',
            dataStructure: 'BBBBBB',
          },
          new ExperimentMetaData()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ExperimentMetaData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            description: 'BBBBBB',
            dataStructure: 'BBBBBB',
            viewerClasses: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ExperimentMetaData', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addExperimentMetaDataToCollectionIfMissing', () => {
        it('should add a ExperimentMetaData to an empty array', () => {
          const experimentMetaData: IExperimentMetaData = { id: 'ABC' };
          expectedResult = service.addExperimentMetaDataToCollectionIfMissing([], experimentMetaData);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(experimentMetaData);
        });

        it('should not add a ExperimentMetaData to an array that contains it', () => {
          const experimentMetaData: IExperimentMetaData = { id: 'ABC' };
          const experimentMetaDataCollection: IExperimentMetaData[] = [
            {
              ...experimentMetaData,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addExperimentMetaDataToCollectionIfMissing(experimentMetaDataCollection, experimentMetaData);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ExperimentMetaData to an array that doesn't contain it", () => {
          const experimentMetaData: IExperimentMetaData = { id: 'ABC' };
          const experimentMetaDataCollection: IExperimentMetaData[] = [{ id: 'CBA' }];
          expectedResult = service.addExperimentMetaDataToCollectionIfMissing(experimentMetaDataCollection, experimentMetaData);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(experimentMetaData);
        });

        it('should add only unique ExperimentMetaData to an array', () => {
          const experimentMetaDataArray: IExperimentMetaData[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'Soap' }];
          const experimentMetaDataCollection: IExperimentMetaData[] = [{ id: 'ABC' }];
          expectedResult = service.addExperimentMetaDataToCollectionIfMissing(experimentMetaDataCollection, ...experimentMetaDataArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const experimentMetaData: IExperimentMetaData = { id: 'ABC' };
          const experimentMetaData2: IExperimentMetaData = { id: 'CBA' };
          expectedResult = service.addExperimentMetaDataToCollectionIfMissing([], experimentMetaData, experimentMetaData2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(experimentMetaData);
          expect(expectedResult).toContain(experimentMetaData2);
        });

        it('should accept null and undefined values', () => {
          const experimentMetaData: IExperimentMetaData = { id: 'ABC' };
          expectedResult = service.addExperimentMetaDataToCollectionIfMissing([], null, experimentMetaData, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(experimentMetaData);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

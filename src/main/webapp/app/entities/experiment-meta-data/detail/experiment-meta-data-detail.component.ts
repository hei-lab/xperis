import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExperimentMetaData } from '../experiment-meta-data.model';
import { DataUtils } from 'app/core/util/data-util.service';
import { ExperimentMetaDataService } from 'app/entities/experiment-meta-data/service/experiment-meta-data.service';
import { IPlugin } from 'app/entities/plugin/plugin.model';
import { PluginService } from 'app/entities/plugin/service/plugin.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-experiment-meta-data-detail',
  templateUrl: './experiment-meta-data-detail.component.html',
})
export class ExperimentMetaDataDetailComponent implements OnInit {
  experimentMetaData: IExperimentMetaData | null = null;
  plugin?: IPlugin[];

  constructor(
    protected experimentMetaDataService: ExperimentMetaDataService,
    protected pluginService: PluginService,
    protected dataUtils: DataUtils,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ experimentMetaData }) => {
      this.experimentMetaData = experimentMetaData;
    });
    this.pluginService.queryAll(this.experimentMetaData?.dataMimeType?.id).subscribe(
      (res: HttpResponse<IPlugin[]>) => {
        this.plugin = res.body ?? [];
      },
      () => {
        this.plugin = [];
      }
    );
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  downloadZip(id: string | undefined): void {
    if (id) {
      this.experimentMetaDataService.getZip(id).subscribe(res => {
        this.dataUtils.openFileBlob(res, 'application/zip', id);
      });
    }
  }
  previousState(): void {
    window.history.back();
  }

  getPluginView(id: string | undefined, plugin: IPlugin): void {
    if (id && plugin.name) {
      this.experimentMetaDataService.getView(id, plugin.name).subscribe(res => {
        this.dataUtils.openFileBlob(res, plugin.dataMimeType?.type, id);
      });
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IExperimentMetaData, ExperimentMetaData } from '../experiment-meta-data.model';
import { ExperimentMetaDataService } from '../service/experiment-meta-data.service';

@Injectable({ providedIn: 'root' })
export class ExperimentMetaDataRoutingResolveService implements Resolve<IExperimentMetaData> {
  constructor(protected service: ExperimentMetaDataService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExperimentMetaData> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((experimentMetaData: HttpResponse<ExperimentMetaData>) => {
          if (experimentMetaData.body) {
            return of(experimentMetaData.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ExperimentMetaData());
  }
}

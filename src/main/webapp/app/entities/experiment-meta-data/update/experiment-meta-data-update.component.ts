import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ExperimentMetaData, IExperimentMetaData } from '../experiment-meta-data.model';
import { ExperimentMetaDataService } from '../service/experiment-meta-data.service';
import { EventManager } from 'app/core/util/event-manager.service';
import { DataUtils } from 'app/core/util/data-util.service';
import { IExperiment } from 'app/entities/experiment/experiment.model';
import { ExperimentService } from 'app/entities/experiment/service/experiment.service';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { DataMimeTypeService } from 'app/entities/data-mime-type/service/data-mime-type.service';
import { IDataTemplate } from 'app/entities/data-template/data-template.model';

@Component({
  selector: 'jhi-experiment-meta-data-update',
  templateUrl: './experiment-meta-data-update.component.html',
})
export class ExperimentMetaDataUpdateComponent implements OnInit {
  isSaving = false;

  experimentsSharedCollection: IExperiment[] = [];
  dataMimeTypesSharedCollection: IDataMimeType[] = [];

  editForm = this.fb.group({
    id: [],
    description: [null, [Validators.required]],
    dataStructure: [null, [Validators.required]],
    viewerClasses: [],
    experiment: [null, Validators.required],
    experimentName: [],
    dataMimeType: [null, Validators.required],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected experimentMetaDataService: ExperimentMetaDataService,
    protected experimentService: ExperimentService,
    protected dataMimeTypeService: DataMimeTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ experimentMetaData }) => {
      if ('viewerClasses' in history.state) {
        this.fillForm(history.state);
      } else {
        this.updateForm(experimentMetaData);
      }
      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const experimentMetaData = this.createFromForm();
    if (experimentMetaData.id) {
      this.subscribeToSaveResponse(this.experimentMetaDataService.partialUpdate(experimentMetaData));
    } else {
      this.subscribeToSaveResponse(this.experimentMetaDataService.create(experimentMetaData));
    }
  }

  trackExperimentById(index: number, item: IExperiment): string {
    return item.id!;
  }

  trackDataMimeTypeById(index: number, item: IDataMimeType): string {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExperimentMetaData>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(experimentMetaData: IExperimentMetaData): void {
    this.editForm.patchValue({
      id: experimentMetaData.id,
      description: experimentMetaData.description,
      dataStructure: experimentMetaData.dataStructure,
      viewerClasses: experimentMetaData.viewerClasses,
      experiment: experimentMetaData.experiment,
      experimentName: experimentMetaData.experiment?.name,
      dataMimeType: experimentMetaData.dataMimeType,
    });

    this.experimentsSharedCollection = this.experimentService.addExperimentToCollectionIfMissing(
      this.experimentsSharedCollection,
      experimentMetaData.experiment
    );
    this.dataMimeTypesSharedCollection = this.dataMimeTypeService.addDataMimeTypeToCollectionIfMissing(
      this.dataMimeTypesSharedCollection,
      experimentMetaData.dataMimeType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.experimentService
      .query()
      .pipe(map((res: HttpResponse<IExperiment[]>) => res.body ?? []))
      .pipe(
        map((experiments: IExperiment[]) =>
          this.experimentService.addExperimentToCollectionIfMissing(experiments, this.editForm.get('experiment')!.value)
        )
      )
      .subscribe((experiments: IExperiment[]) => (this.experimentsSharedCollection = experiments));

    this.dataMimeTypeService
      .queryAll()
      .pipe(map((res: HttpResponse<IDataMimeType[]>) => res.body ?? []))
      .pipe(
        map((dataMimeTypes: IDataMimeType[]) =>
          this.dataMimeTypeService.addDataMimeTypeToCollectionIfMissing(dataMimeTypes, this.editForm.get('dataMimeType')!.value)
        )
      )
      .subscribe((dataMimeTypes: IDataMimeType[]) => (this.dataMimeTypesSharedCollection = dataMimeTypes));
  }

  protected createFromForm(): IExperimentMetaData {
    return {
      ...new ExperimentMetaData(),
      id: this.editForm.get(['id'])!.value,
      description: this.editForm.get(['description'])!.value,
      dataStructure: this.editForm.get(['dataStructure'])!.value,
      viewerClasses: this.editForm.get(['viewerClasses'])!.value,
      experiment: this.editForm.get(['experiment'])!.value,
      dataMimeType: this.editForm.get(['dataMimeType'])!.value,
    };
  }

  private fillForm(dataTemplate: IDataTemplate): void {
    this.editForm.patchValue({
      dataStructure: dataTemplate.dataStructure,
      viewerClasses: dataTemplate.viewerClasses,
      dataMimeType: dataTemplate.dataMimeType,
    });
  }
}

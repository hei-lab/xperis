---
layout: default
---

# INSTALLATION

1. Clone the repository locally
```
git clone git@gitlab.com:hei-lab/xperis.git
```

2. Build the project locally
```
./mvnw clean install -DskipTests -Pprod
```

3. Connect to the server
```
ssh 192.168.224.2 -l heilab
```

3.1 install Java 11
```
sudo apt install openjdk-11-jdk
```

3.2 install mongodb as described in the link below
https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-debian/

4. Create the directories for the application in the server (/opt/xperis)
```
sudo mkdir /opt/xperis
sudo mkdir /opt/xperis/conf/tls
sudo mkdir /opt/xperis/conf/plugins
```

5. From your local machine copy files to the server:\
5.1 JAR created in step 2
```
scp ./target/xperis-1.0.3.jar heilab@192.168.224.2:/home/heilab/xperis.jar
```

5.2 service script file 
```
scp ./conf/xperis.service heilab@192.168.224.2:/home/heilab
```

5.3 certificate self signed to enable TLS
```
scp ./conf/keystore.p12  heilab@192.168.224.2:/home/heilab
```

6. Again in the server, change the owner of the files to root
```
sudo chown root:root /opt/xperis/xperis.jar
sudo chown root:root /opt/xperis/xperis.service
sudo chown root:root /opt/xperis/keystore.p12
```

7. Copy files to the application directory
```
sudo cp /home/heilab/xperis.jar /opt/xperis/xperis.jar
sudo cp /home/heilab/keystore.p12 /opt/xperis/conf/tls
```

8. Change environment variables in the service file according to your needs
```
Environment="SPRING_DATA_MONGODB_URI=mongodb://localhost:27017"
Environment="SPRING_DATA_MONGODB_DATABASE=xperis"
Environment="SPRING_MAIL_HOST=smtp.test.com"
Environment="SPRING_MAIL_PORT=587"
Environment="SPRING_MAIL_USERNAME=xperis@test.com"
Environment="SPRING_MAIL_PASSWORD=password"
Environment="JHIPSTER_MAIL_BASE-URL=https://xperis.test.com"
Environment="JHIPSTER_MAIL_FROM=xperis@test.com"
```

9. install service file into systemd
```
sudo cp /opt/xperis/xperis.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable xperis
```

10. start the application
```
sudo systemctl start xperis
```

---
layout: default
title: Home
---

# Xperis HCI Plataform

Xperis HCI is a platform to assist psychology professionals in obtaining, storing and managing data from human interaction experiments through the computer. It was developed in the most modern microservices architecture in the Java language with Angular.
The HCI shall consist of:

- Unified system of authentication and experiment logging
- Storage system for the various types of data captured in the experiments, such as questionnaire responses, patient actions, bio-feedback, audio and video.
- Full experiment management API for integration with data entry programs (usually developed in Unity)

## Documentation <a name = "documentation"></a>

More details about the system can be found at:

- [User’s guide](userguide.html)
- [API documentation](generated-sources/swagger-html/index.html) (Dynamic version [here](http://localhost:9000/swagger-ui/index.html){:target="\_blank"})
- [Java Documentation](site/apidocs/index.html)
- [Installation guide](installation.html)

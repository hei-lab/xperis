#!/bin/bash

# Set the maximum width
MAX_WIDTH=800

# Loop through all the files in the directory
for file in *.jpg *.jpeg *.png *.gif; do
    # Get the width of the image
    WIDTH=$(identify -format "%w" "$file")

    # Check if the image needs to be resized
    if [ "$WIDTH" -gt "$MAX_WIDTH" ]; then
        # Calculate the new height based on the maximum width
        HEIGHT=$(identify -format "%h" "$file")
        NEW_HEIGHT=$(( $HEIGHT * $MAX_WIDTH / $WIDTH ))

        # Resize the image using ImageMagick
        convert "$file" -resize "${MAX_WIDTH}x${NEW_HEIGHT}" "$file"
        echo "Resized $file"
    fi
done

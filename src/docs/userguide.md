---
layout: default
---

# Xperis User's Guide

Xperis is the control system for human interaction experiments of the Hei-lab laboratory of the Lusófona University developed by the student Marcelo Alves during the defense of his Master's thesis in Computer Engineering in the year 2023.

## Table of Contents

- [Profiles](#profiles)
- [User Registration](#user-registration)
    - [Create a new User](#create-a-new-user)
    - [New User Validation](#new-user-validation)
    - [Create Master User](#create-master-user)
- [Institution](#institution)
    - [Create a new Institution](#create-a-new-institution)
    - [Edit Institution](#edit-institution)
    - [Delete Institution](#delete-institution)
    - [View Institution](#view-institution)
- [Experiment](#experiment)
    - [Create Experiment](#create-experiment)
    - [Edit Experiment](#edit-experiment)
    - [Delete Experiment](#delete-experiment)
    - [View Experiment](#view-experiment)
- [Experiment Metadata](#experiment-metadata)
    - [Create Metadata](#create-metadata)
    - [Create Metadata with Template](#create-metadata-with-template)
    - [Edit Metadata](#edit-metadata)
    - [Delete Metadata](#delete-metadata)
    - [View Metadata](#view-metadata)
    - [Download Metadata](#download-metadata)
    - [Use Plugin](#use-plugin)
- [Data Type Template](#data-type-template)
    - [Create Data Type Template](#create-data-type-template)
    - [Edit Data Type Template](#edit-data-type-template)
    - [Delete Data Type Template](#delete-data-type-template)
    - [View Data Type Template](#view-data-type-template)
    - [Create Experiment Metadata from Data Type Template](#create-experiment-metadata-from-data-type-template)
- [Experiment Data](#experiment-data)
    - [Create Experiment Data](#create-experiment-data)
    - [Edit Experiment Data](#edit-experiment-data)
    - [Delete Experiment Data](#delete-experiment-data)
    - [View Experiment Data](#view-experiment-data)
    - [Download Experiment Data](#download-experiment-data)
- [Plugin](#plugin)
    - [Add Plugin](#add-plugin)
    - [Edit Plugin](#edit-plugin)
    - [Deactivate Plugin](#deactivate-plugin)
    - [Activate Plugin](#activate-plugin)
    - [Delete Plugin](#delete-plugin)
    - [View Plugin](#view-plugin)
    - [Download Plugin](#download-plugin)



## Profiles

The system has 3 access profiles:

- ADMIN: user with access to all features and responsible for administering all other users
- MASTER: user with permission to create experiments, initiate data collection and authorize other participants in the system. He can also be a participant in another MASTER user's experiment, and in this case he does not have access to modify the experiment, he only has read access
- USER: user with permission only to participate in experiments as a data collector. This user has read-only access to the experiment data and is generally responsible for entering the data into the system.


## User Registration
### _Create a new User_
1. Navigate to home\
![alt_text](assets/images/welcome.png)\
2. Choose the option Register in the Account menu on the top right, or choose the "Create a new account" option in the Authentication 
4. The registration screen will open\
![alt_text](assets/images/image01.png)\
5. Fill username, email address and a password.
6. Pressing register it will trigger a verification email to the entered address.
7. The email will look like this\
![alt_text](assets/images/image02.png)\
8. After clicking the link in the email, the account will be validated.

### _New User Validation_

However, thar user creation still need the ADMIN's approval.
1. Login as administrator
2. Go to the Administration menu and choose "User Management" option 
3. Find the new user and press the "Inactive" button in front of the user's email address to activate it.
![alt_text](assets/images/image03.png)\
4. Only after Activated, the user in question will be able to log in to the application
Every newly created user gets the USER profile.

### _Create Master User_

To add new MASTER users, the process should be done the same way, 
After the ADMIN approve it, still need to change the profile manually:
1. in the "User Management" choose the "Edit" button. 
2. Change the profile to MASTER and press "Save"\
![alt_text](assets/images/image04.png)\
3. On this screen you can also change the user's name and nickname.


## Institution
### _Create a new Institution_
1. Login as MASTER user
2. Go to the Entities menu and choose "Institution" option
3. Choose the "Create Institution" option
4. Will open the screen "Create or edit Institution"\
![alt_text](assets/images/image17.png)\
5. Fill the name and choose the employees of the institution in the list of already registered users.
6. After you have entered the data and pressed the "Save" button, the system returns to the list of Institutions and displays the basic data as well as the options to "View", "Edit" and "Delete".\
![alt_text](assets/images/image18.png)\

### _Edit Institution_
   To change any of the data entered when creating the Institution:
1. Choose the "Edit" option directly from the "Institution" list
2. The screen "Create or edit Institution" will open again, with the data already entered.
3. To remove a user from the Employees list, just click with the mouse and hold the CTRL key.
4. By choosing "Save" the changes will take effect
5. It is possible to cancel the process just by pressing the "Cancel" button

### _Delete Institution_
To delete an Institution
1. Choose the "Delete" option directly from the "Institution" list
2. A confirmation screen will appear.\
![alt_text](assets/images/image19.png)\
3. If you choose "Delete" the Institution will be permanently deleted. If you choose "Cancel" the Institution will not be deleted.

### _View Institution_
To view the details of an Institution
1. Choose the "View" option directly from the "Institution" list
2. The screen "View Institution" will open 
![alt_text](assets/images/image20.png)\
3. In addition to the basic data, , this screen will also show the date/time stamp of the creation and of the last change.
4. It is possible to return to the list of Institutions by pressing the "Back" button


## Experiment
### _Create Experiment_
To create a new experiment:
1. Login as MASTER user
2. Go to the Entities menu and choose "Experiment" option
3. Choose the "Create Experiment" option (For a user with the profile USER this option will not be visible)
4. Will open the screen "Create or edit Experiment"\
5. Fill the name, the description, the start date of the experiment, as well as a forecast of the start and end of the data collection.\
![alt_text](assets/images/image05.png)\
6. On this screen you can choose other MASTER users to have access to the experiment, called members. And you can also choose the assistants that will have access to collect data (those with the USER profile)
7. After you have entered the data and pressed the "Save" button, the system returns to the list of Experiments and displays the basic data as well as the options to "View", "Edit" and "Delete".\
![alt_text](assets/images/image06.png)\
The status of the experiment is automatically obtained according to the dates entered. 
For those experiments that already have metadata created will be displayed also a direct link to each metadata.

### _Edit Experiment_
To change any of the data entered when creating the experiment:
1. Choose the "Edit" option directly from the "Experiments" list
2. The screen "Create or edit Experiment" will open again, with the data already entered.
3. To remove a user from the "members" or "assistants" list, just click with the mouse and hold the CTRL key.
4. By choosing "Save" the changes will take effect
5. It is possible to cancel the process just by pressing the "Cancel" button

### _Delete Experiment_
To delete an experiment
1. Choose the "Delete" option directly from the "Experiments" list
2. A confirmation screen will appear\
![alt_text](assets/images/image07.png)\
3. If you choose "Delete" the experiment will be permanently deleted. If you choose "Cancel" the process will be canceled and the experiment will not be deleted.

### _View experiment_
To view all the data entered when creating the experiment
1. Choose the "View" option directly from the "Experiments" list
2. The screen "View Experiment" will open
![alt_text](assets/images/image08.png)\
3. In addition to the initial data entered, this screen will also show the date/time stamp of the creation and of the last change. Also, in the "Metadata" section, a link to the metadata associated with the experiment will appear.
4. It is possible to return to the list of Experiments by pressing the "Back" button


## Experiment Metadata
### _Create Metadata_
An experiment can collect various different types of data like questionnaires, electronic data feedbacks, impulses, and more. In order to create a new metadata type associated with an experiment:
1. Login as MASTER user
2. Go to the Entities menu and choose "Experiment Metadata" option
3. Choose the "Create new Experiment MetaData" option (For a user with the profile USER this option will not be visible)
4. Will open the screen "Create or edit Experiment MetaData"
   ![alt_text](assets/images/image09.png)\
5. Choose Experiment, fill the description, the data structure and choose the data mime type.\
6. After you have entered the data and pressed the "Save" button, the system returns to the list of Experiments and displays the basic data as well as the options to "View", "Edit" and "Delete"\
   ![alt_text](assets/images/image10.png)\
   For those metadata that already have collected data will be displayed also  "Download Zip" button to download all data in a compressed file.
   And for those metadata with a data mime type that already have a plugin associated, will be displayed also a button to each plugin in order to be able to use it.  
   
### _Create Metadata with Template_
In order to create a new metadata using a template:
1. Go to the Entities menu and choose "Experiment Metadata" option
2. Choose the "Create from Template" option (For a user with the profile USER this option will not be visible)
3. Will open the screen "Create or edit Experiment MetaData"
4. Choose Experiment and fill the description. The data structure and the data mime type will be automatically filled with the data from the template.
5. After you have entered the data and pressed the "Save" button, the system returns to the list of Experiments and displays the basic data as well as the options to "View", "Edit" and "Delete".

### _Edit Metadata_
To change any of the data entered when creating the experiment metadata:
1. Choose the "Edit" option directly from the "Experiment Metadata" list
2. The screen "Create or edit Experiment Metadata" will open again, with the data already entered.
3. By choosing "Save" the changes will take effect
4. It is possible to cancel the process just by pressing the "Cancel" button

### _Delete Metadata_
To delete an experiment metadata
1. Choose the "Delete" option directly from the "Experiment Metadata" list
2. A confirmation screen will appear\
   ![alt_text](assets/images/image11.png)\
3. If you choose "Delete" the experiment metadata will be permanently deleted. 
   If you choose "Cancel" the process will be canceled and the experiment metadata will not be deleted.

### _View Metadata_
To view all the data entered when creating the experiment metadata
1. Choose the "View" option directly from the "Experiment Metadata" list
2. The screen "View Experiment" will open\
   ![alt_text](assets/images/image12.png)\
In addition to the initial data entered, this screen will also show the date/time stamp of the creation and of the last change. 
In the "Experiment Data" section, a link to the data associated with the experiment metadata will appear.
Also, it will appear a change log section, where will be displayed all changes made to the metadata, with the date/time stamp and the user who made the change.
3. It is possible to return to the list of Experiments Metadata by pressing the "Back" button

### _Download Metadata_
To download all data associated with the experiment metadata
1. Choose the "Download Zip" option directly from the "Experiment Metadata" list or from the "View Experiment Metadata" screen
2. A compressed file will be downloaded with all data associated with the experiment metadata.

### _Use Plugin_
To use a plugin associated with the experiment metadata
1. Choose the "Plugin" directly from the "Experiment Metadata" list
2. A file generated by the plugin will be downloaded with all collected data associated with the experiment metadata.


## Data Type Template
### _Create Data Type Template_
To create a new data type template to utilize in the creation of metadata:
1. Login as MASTER user
2. Go to the Entities menu and choose "Data Type Template" option
3. Choose the "Create new Data Type Template" option (For a user with the profile USER this option will not be visible)
4. Will open the screen "Create or edit Data Type Template"
![alt_text](assets/images/image13.png)\
5. Fill the name, the description, the data structure and choose the data mime type.\
6. After you have entered the data and pressed the "Save" button, the system returns to the list of Data Type Templates and displays the basic data as well as the options to "View", "Edit" and "Delete"\
![alt_text](assets/images/image14.png)\

### _Edit Data Type Template_
To change any of the data entered when creating the data type template:
1. Choose the "Edit" option directly from the "Data Type Template" list
2. The screen "Create or edit Data Type Template" will open again, with the data already entered.
3. By choosing "Save" the changes will take effect
4. It is possible to cancel the process just by pressing the "Cancel" button

### _Delete Data Type Template_
To delete a data type template
1. Choose the "Delete" option directly from the "Data Type Template" list
2. A confirmation screen will appear\
![alt_text](assets/images/image15.png)\
3. If you choose "Delete" the data type template will be permanently deleted. 
   If you choose "Cancel" the process will be canceled and the data type template will not be deleted.

### _View Data Type Template_
To view all the data entered when creating the data type template
1. Choose the "View" option directly from the "Data Type Template" list
2. The screen "View Data Type Template" will open
![alt_text](assets/images/image16.png)\
3. In addition to the initial data entered, this screen will also show the date/time stamp of the creation and of the last change. 
As well as the "Create Metadata" button.

### _Create Experiment Metadata from Data Type Template_
To create a new experiment metadata using a data type template:
1. Choose the "Metadata" option directly from the "Data Type Template" list or in the "View Data Type Template" screen.
2. Will open the screen "Create or edit Experiment MetaData"
3. Choose Experiment and fill the description. The data structure and the data mime type will be automatically filled with the data from the template.
4. After you have entered the data and pressed the "Save" button, the system returns to the list of Experiments Metadata.


## Experiment Data
### _Create Experiment Data_
To create a new experiment data:
1. Login as MASTER user
2. Go to the Entities menu and choose "Experiment Data" option
3. Choose the "Create new Experiment Data" option (For a user with the profile USER this option will not be visible)
4. Will open the screen "Create or edit Experiment Data"
![alt_text](assets/images/image21.png)\
5. Fill the participants identifier (in order to hide the participant identity), the experiment metadata and the upload the data file. 
The file must be in the same format as informed in the data mime type field of the experiment metadata. 
6. After you have entered the data and pressed the "Save" button, the system returns to the list of Experiment Data and displays the basic data as well as the options to "View", "Edit" and "Delete"\
![alt_text](assets/images/image22.png)\

### _Edit Experiment Data_
To change any of the data entered when creating the experiment data:
1. Choose the "Edit" option directly from the "Experiment Data" list
2. The screen "Create or edit Experiment Data" will open again
3. By choosing "Save" the changes will take effect
4. It is possible to cancel the process just by pressing the "Cancel" button

### _Delete Experiment Data_
To delete an experiment data
1. Choose the "Delete" option directly from the "Experiment Data" list
2. A confirmation screen will appear\
![alt_text](assets/images/image23.png)\
3. If you choose "Delete" the experiment data will be permanently deleted. 
   If you choose "Cancel" the process will be canceled and the experiment data will not be deleted.

### _View Experiment Data_
To view all the data entered when creating the experiment data
1. Choose the "View" option directly from the "Experiment Data" list
2. The screen "View Experiment Data" will open
![alt_text](assets/images/image24.png)\
3. In addition to the initial data entered, this screen will also show the date/time stamp of the creation and of the last change.
   Also, it will appear a change log section, where will be displayed all changes made to the metadata, with the date/time stamp and the user who made the change.
4. It is possible to return to the list of Experiments Data by pressing the "Back" button
 
### _Download Experiment Data_
To download the file associated with the experiment data
1. Choose the "Download" button directly from the "Experiment Data" list or from the "View Experiment Data" screen
2. The file associated with the experiment data will be downloaded.
3. It is possible to return to the list of Experiments Data by pressing the "Back" button


## Plugin
### _Add Plugin_
To add a new plugin:
1. Login as ADMIN user
2. Go to the Administration menu and choose the "Plugins" option
3. Choose the "Add new Plugin" button 
4. Will open the screen "Add or edit a Plugin"
![alt_text](assets/images/image25.png)\
5. Fill the name, the class name, choose the data mime type and upload the plugin file. 
The file must be a .jar file. This plugin needs to be developed using the plugin core library implementing the interface "PluginInterface" and the method "executePlugin".
6. After you have entered the data and pressed the "Save" button, the system returns to the list of Plugins and displays the basic data as well as the options to "View", "Edit" and "Delete"\
![alt_text](assets/images/image26.png)\
   At this moment the plugin will be validated and loaded to the application in order to be available to be used in the experiment metadata screen.
   If the plugin is not a valid implementation, a red cross will be displayed after the class name, the status will be changed to "Invalid" and the plugin will not be loaded. 
   If the plugin is valid, a green check mark will be displayed after the class name, the status will be changed to "Activated" and the plugin will be loaded.

### _Edit Plugin_
To change any of the data entered when adding the plugin:
1. Choose the "Edit" option directly from the "Plugins" list
2. The screen "Add or edit a Plugin" will open again, with the data already entered. 
   It is possible to change the name, the class name, the data mime type and the plugin file. For a valid plugin is able to change the status of the plugin from activated to inactivated and vice versa.
3. By choosing "Save" the changes will take effect
4. It is possible to cancel the process just by pressing the "Cancel" button

### _Deactivate Plugin_
To deactivate a plugin:
1. Click at "Activated" button after plugin name directly from the "Plugins" list. This option will not be available for plugins with status "Invalid".
2. The plugin will be deactivated and the status will be changed to "Deactivated"
![alt_text](assets/images/image27.png)\

### _Activate Plugin_
To re-activate a plugin:
1. Click at "Deactivated" button after plugin name directly from the "Plugins" list. This option will not be available for plugins with status "Invalid".
2. The plugin will be re-activated and the status will be changed to "Activated"

### _Delete Plugin_
To delete a plugin
1. Choose the "Delete" option directly from the "Plugins" list
2. A confirmation screen will appear\
![alt_text](assets/images/image28.png)\
3. If you choose "Delete" the plugin will be permanently deleted. 
   If you choose "Cancel" the process will be canceled and the plugin will not be deleted.

### _View Plugin_
To view all the data entered when adding the plugin
1. Choose the "View" option directly from the "Plugins" list
2. The screen "View Plugin" will open\
![alt_text](assets/images/image29.png)\
3. In addition to the initial data entered, this screen will also show the date/time stamp of the creation and of the last change.
4. It is possible to return to the list of Plugins by pressing the "Back" button

### _Download Plugin_
To download the jar file associated with the plugin
1. Choose the "Download" button from the "View Plugin" screen
2. The jar file associated with the plugin will be downloaded.



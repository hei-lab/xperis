# Xperis HCI Plataform

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Support](#support)
- [License](#license)

## About <a name = "about"></a>

Xperis HCI is a platform to assist psychology professionals in obtaining, storing and managing data from human interaction experiments through the computer. It was developed in the most modern microservices architecture in the Java language with Angular.
The HCI shall consist of:

- Unified system of authentication and experiment logging
- Storage system for the various types of data captured in the experiments, such as questionnaire responses, patient actions, bio-feedback, audio and video.
- Full experiment management API for integration with data entry programs (usually developed in Unity)

## Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

To build the software you need to have installed on the computer:

- Java (11+)
- Node.js (12+)
- Maven (3.3+)
- MongoDB (4.4+)

### Installing

First clone this repository

```
git clone https://gitlab.com/hei-lab/xperis.git
```

After that, to build this project, you must install and configure the following dependencies on your machine:

1. [Node.js](https://nodejs.org/): We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

```
npm install
```

We use npm scripts and [Angular CLI](https://cli.angular.io/) with [Webpack](https://webpack.github.io/) as our build system.

Run the following commands in two separate terminals to create a blissful development experiment where your browser
auto-refreshes when files change on your hard drive.

```
./mvnw
npm start
```

Make sure the mongodb server is running on port 27017.

Npm is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all of the scripts available to run for this project.

The application will be available on http://localhost:9000

## Usage <a name = "usage"></a>

Follow the basic steps:

### Registration

When running the system for the first time, 2 users will be available:

- `admin`, password _admin_ for platform administration
- `user`, password _user_ as the first user - experiment create profile (MASTER)

![welcome](src/docs/assets/images/welcome.png)

Click on `Register the new Account` to register a new user. Enter your email and password. You will receive an email confirmation with the activation link.

This new user initially receives a read only profile. To access the experiments it will be necessary to be confirmed by the administrator.

Enter with admin user, go to Administration menu and selects `User management`. Under new user click on `edit`, change the profile to `ROLE_USER` and save. When backs to user list, again unde user selcted click on `Inactivated` buton to activate it.

### Define Experiments

Log out from admin user and login with new user

To create a new Experiment, go ot the Entities menu and choose Experiment. It will be displayed a list of existent experiments. And then click on button `Create a new Experiment`
Fill the name, description, start date, finish capture date and experiment final date. This represent a experiment master record.

After you need create one metaData to each data type will be captured. For this enter at `Experiment Meta Data` option at the menu Entities. Here, you should choose the correct dataType that your experiment will generate.

The Experiment Classes Diagram:

![Diagram](src/docs/assets/images/diagram-puml.png)

Now you have all information to send data to plataform throw the API. See more details in documentation link below.

### Send Data

Xperis HCI provides a complete interface (API) for connecting any application that wants to store data from a human interaction experiment.

To send the collected data to each participant of an experiment one can use the service Createexperimentdata. Here is an example of the call using CURL:

```
curl --location --request POST 'http://localhost:8080/api/experiment-data' \
--header 'user-api-key: 61425538d5a03d512659c6fb' \
--header 'Content-Type: application/json' \
--data-raw '{
  "participants" : "D04",
  "data" : "MTk7ZmVtYWxlO1BvcnR1Z3Vlc2U7NDsxOzE=",
  "dataContentType" : "text/csv",
  "experimentMetaData" : {
    "id" : "6182046cb28165018a7aeb56"
  }
}'
```

In this example:

- `id` is the id of ExperimentMetaData registered in the platform, for that refers the data
- `user-api-key` is the key of user responsible for the experiment or a experiment member related
- `dataContentType` is the type of data that will be sent and must respect the one that was set for the ExperimentMetaData
- `data` is the content of the file/data encoded in Base64.

## Support <a name = "support"></a>

More details about the system can be found at: [https://hei-lab.gitlab.io/xperis](https://hei-lab.gitlab.io/xperis)

## License <a name = "licence"></a>

This project is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).
